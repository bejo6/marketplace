<?php

	set_time_limit(0);
	define('DEBUG', true);
	define('WRITELOG', true);

	function myheader($filename){
		echo "=============================================\n";
		echo "[*] Qlobot Duplicate/Blacklist Keyword Removal\n";
		echo "[*] Author     : Ryan Aby\n";
		echo "[*] Recoded by : bejo6\n";
		echo "[*] Usage      : php $filename <file name>\n";
		echo "[*] Example    : php $filename fab24e7e5d8d38ee948cfb75b2525209.csv\n";
		echo "=============================================\n\n";
	}
	
	function fecho($value){
		if ( DEBUG && $value ) {
			if ( is_array($value) || is_object($value) ) {
				foreach ( $value as $key=>$val ) {
					if ( is_array($val) || is_object($value) ) {
						fecho($val);
					} else {
						if ( is_numeric($key) || $key == 'scalar' ) {
							echo "{$val}\n";
						} else {
							echo "{$key} : {$val}\n";
						}
					}
				}
			} else {
				echo "{$value}\n";
			}
		}
	}
	
	function write_log($file, $str) {
		if ( WRITELOG ) {
			$fp = fopen($file,'a');
			fwrite($fp,"{$str}\r\n");
			fclose($fp);
		}
	}
	
	function make_key($str){
		$str = preg_replace('/\[(.*?)\]|\((.*?)\)|\{(.*?)\}/','',$str);
		$str = preg_replace('/(reseller welcome|(best( +)?)?(sale|seller)|obral|hot|sale|promo|grosir|((ter)?(murah|baru)(( +)?banget)?)|diskon|bonus|ori(ginal)?|asli|new)/i','',$str);
		$str = preg_replace('/sku([\s:]+)?\d+/i','',$str);

		/* Mainboard
		$str = preg_replace('/(garansi\s+)?resmi\s+\d+(\s+)?(tahun|thn?)/i','',$str);
		$str = preg_replace('/motherboard|mainboard/i','',$str);
		$str = preg_replace('/(ddr|usb|sata)[\d\s\.]+/i','',$str);
		*/

		/* Phone
		$str = preg_replace('/garansi|grs|resmi?|distri(butor)?|\d+(\s+)?(tahun|thn?)/i','',$str);
		$str = preg_replace('/(^|\s+)(smartphone|handphone|hp\s+|segel)/i','',$str);
		$str = preg_replace('/oppo|xiaomi|samsung|vivo|asus|indonesia|tam|sein|bnib/i','',$str);
		$str = preg_replace('/case|screen|guard|selfie|free|tempered|glass/i','',$str);
 		*/
		
		/* Size/Ukuran
		$str = preg_replace('/\W+(S|M|L|X+L(\W+)\d+)(\W+)?/i','',$str);
		*/
		
		/* Fashion anak */
		$str = preg_replace('/(kaos|baju|pakaian)(\s+)?(anak)?/i','',$str);

		$str = preg_replace('/[\W_]/','',$str);

		return strtolower($str);
	}
	
	function filter_blacklist($str){
		if ( contains_price($str) ) {
			return false;
		}
		if ( contains_link($str) ) {
			return false;
		}
		if ( contains_competitor($str) ) {
			return false;
		}
		if ( contains_phone_number($str) ) {
			return false;
		}
		if ( contains_contact($str) ) {
			return false;
		}

		if ( contains_sosmed($str) ) {
			return false;
		}
		if ( contains_junk($str) ) {
			return false;
		}

		return true;
	}
	
	function contains_link($str){
		$paterns[] = "https?:\/\/([a-z0-9]([a-z0-9-]+)\.)(([a-z0-9][a-z0-9-]+\.)+)?[a-z]{2,}"; // link
		$paterns[] = "(^| )https?:?\/\/|www\.?"; // link
		$paterns[] = "(^| )((web|situs)[\s\.:])?www"; // domain
		
		foreach ( $paterns as $val ) {
			if( preg_match("/{$val}/i", $str, $match) ) {
				fecho("[!] Blacklisted : (link/domain) Contain {$match[0]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
				return true;
			}
		}
		
		$patern = "(^[a-z0-9]([a-z0-9-]+)\.)(([a-z0-9][a-z0-9-]+\.)+)?[a-z]{2,}"; // domain
		if ( preg_match("/{$patern}/i", $str, $match) ) {
			$arr = array_filter(array_map('trim', explode(".", $match[0])));
			if ( $arr ){
				if ( !is_numeric($arr[0]) ) {
					fecho("[!] Blacklisted : (link/domain) Contain {$match[0]} -> {$str}");
					write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
					return true;
				}
			}
		}
		
		return false;
	}
	
	function contains_competitor($str){
		$competitor[] = 's ?h ?(o|0) ?p ?e ?e?';
		$competitor[] = 'k ?a ?s ?k ?u ?s';
		$competitor[] = '(^|:| )bl |b ?u ?k ?a ?l ?a ?p ?a ?k';
		$competitor[] = 't ?(o|0) ?k? ?(o|0)? ?p ?e ?d ?(i ?a ?)?';
		foreach( $competitor as $val ){
			if( preg_match("/{$val}/i", $str, $match) ){
				fecho("[!] Blacklisted : (competitor) Contain {$match[1]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[1]}] -> {$str}");
				return true;
			}
		}
		return false;
	}

	function contains_phone_number($str){
		$contact = "((whatsapp|whatsup|wasap|wa|no(m(o|e)r)?|number|(tele)?phone|telepone?|telp|tlp|hp|handphone|fax|sms|office|cp|cs\d?|call|kontak|hubungi|contact?|cp)([\s-_])?(person|us|kami|ke)?)?([\s:\#;\.])?";

		foreach( code_area() as $k=>$v ){
			$o_code = preg_replace('/^0/', 'o', $k);
			$p_code = preg_replace('/^0/', '\+?62 ?', $k);
			$nol    = preg_replace('/^0/', 'nol', $k);
			$split1 = substr_replace($k, '( |-)', 2, 0);
			$split2 = substr_replace($k, '( |-)', 3, 0);


			$patern = "(^|:| ){$contact}\(? ?({$k}|{$o_code}|{$split1}|{$split2}|{$nol}|{$p_code}) ?\)?[\d]{6,}";
			
			if( preg_match("/{$patern}/i", $str, $match) ){
				fecho("[!] Blacklisted : (phone) Contain CODE AREA {$match[0]}({$v}) -> {$str}");
				write_log("blacklist_match.txt", "{$patern} -> [{$match[0]}] -> {$str}");
				return true;
			}
			
			$patern = "(^|:| ){$contact}\(? ?({$k}|{$o_code}|{$split1}|{$split2}|{$nol}|{$p_code}) ?\)?([\d-\.\s]{6,})";
			
			if( preg_match("/{$patern}/i", $str, $match) ){
				$tmp_num = preg_replace('/\D+/', '', $match[0]);
				$length_ident = strlen($k) + 6;
				$length_num   = strlen($tmp_num);
				if( $length_num >= $length_ident ){
					fecho("[!] Blacklisted : (phone) Contain CODE AREA {$match[0]}({$v}) -> {$str}");
					write_log("blacklist_match.txt", "{$patern} -> [{$match[0]}] -> {$str}");

					return true;
				}
			}
			
			$patern = "(^|:| ){$contact}\(? ?({$k}|{$o_code}|{$split1}|{$split2}|{$nol}|{$p_code})[\d-\.\s]{6,} ?\)?";
			
			if( preg_match("/{$patern}/i", $str, $match) ){
				$tmp_num = preg_replace('/\D+/', '', $match[0]);
				$length_ident = strlen($k) + 6;
				$length_num   = strlen($tmp_num);
				if( $length_num >= $length_ident ){
					fecho("[!] Blacklisted : (phone) Contain CODE AREA {$match[0]}({$v}) -> {$str}");
					write_log("blacklist_match.txt", "{$patern} -> [{$match[0]}] -> {$str}");
					return true;
				}
			}
		}
		
		foreach( code_mobile() as $k=>$v ){
			$o_code = preg_replace('/^0/', 'o', $k);
			$p_code = preg_replace('/^0/', '\+?62', $k);
			$nol    = preg_replace('/^0/', 'nol', $k);
			$split1 = substr_replace($k, '( |-)', 2, 0);
			$split2 = substr_replace($k, '( |-)', 3, 0);
			
			$patern = "(^|:| ){$contact}\(? ?({$k}|{$o_code}|{$split1}|{$split2}|{$nol}|{$p_code})[\d]{6,} ?\)?";
			if( preg_match("/{$patern}/i", $str, $match) ){
				fecho("[!] Blacklisted : (phone) Contain MOBILE NUMBER {$match[0]}({$v}) -> {$str}");
				write_log("blacklist_match.txt", "{$patern} -> [{$match[0]}] -> {$str}");

				return true;
			}
			
			$patern = "(^|:| ){$contact}\(? ?({$k}|{$o_code}|{$split1}|{$split2}|{$nol}|{$p_code})[\d-\.\s]{6,} ?\)?";
			
			if( preg_match("/{$patern}/i", $str, $match) ){
				$tmp_num = preg_replace('/\D+/', '', $match[0]);
				$length_num   = strlen($tmp_num);
				if( $length_num >= 10 ){
					fecho("[!] Blacklisted : (phone) Contain MOBILE NUMBER {$match[0]}({$v}) -> {$str}");
					write_log("blacklist_match.txt", "{$patern} -> [{$match[0]}] -> {$str}");

					return true;
				}
			}
		}
		
		return false;
	}

	function contains_contact($str){
		$paterns[] = "(^| )((no(m(o|e)r)?|number)([\s\.:])?)?(telp|telepone?|telephone|phone|tlp|fax|sms|office|cp)([\s\.:\/]+)?\+?[\d]{3,}"; // contact
		$paterns[] = "(^| )(call|kontak|hubungi|contact?|cp)([\s-_])?(person|us|kami)?([\s\.:\/]+)?[\d-\.\s]+"; // contact
		$paterns[] = "(^|\/| )(wa|sms|telp|telepone?|telephone|phone|tlp) only"; // contact
		$paterns[] = "(^| )\@(yahoo|gmail|live|hotmail|outlook|proton|mail|googlemail)"; // mail
		$paterns[] = "(^| )e?mail([\s:])(^[a-z0-9]([a-z0-9-]+)\.)(([a-z0-9][a-z0-9-]+\.)+)?[a-z]{2,}"; // mail

		foreach ( $paterns as $val ) {
			if( preg_match("/{$val}/i", $str, $match) ) {
				fecho("[!] Blacklisted : (contact) Contain {$match[0]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
				return true;
			}
		}
		
		return false;
	}

	function contains_price($str){

		$mlyr = "[1-9]{1}(\d{1,2})?[,.\s]?\d{3}[,.\s]?\d{3}[,.\s]?\d{3}"; // milyar
		$jt   = "[1-9]{1}(\d{1,2})?[,.\s]?\d{3}[,.\s]?\d{3}"; // juta
		$rb   = "[1-9]{1}(\d{1,2})?[,.\s]?\d{3}"; // ribu
		$rts  = "[1-9]{1}\d{2}"; // ratus
		$nom  = "[1-9]{1}(\d{1,2})?"; // nominal
		$xnom  = "(ribu|rb|juta|jt|k(?![\S]))"; 
		
		$paterns[] = "(^|\W|\s+)(rp|idr|\\$|\\@((\s+)?rp)?|=)([\s.]+)?({$jt}|{$rb}|{$nom}(\s+)?{$xnom})"; // price
		$paterns[] = "(^|\W|\s+)(ha?rga?((\s+)?(satuan|per ?(cent(i|y))meter|per (m|cm|mm))(\s+)?)?|price|only|hanya|retail|mulai)([\s.:=]+)?({$jt}|{$rb}|{$nom}(\s+)?{$xnom})"; // price
		$paterns[] = "(^|\W|\s+)(ha?rga?|price|only|hanya|retail|mulai)(.*?)([\s.:=]+)?({$jt}|{$rb}|{$nom}(\s+)?{$xnom})"; // price
		$paterns[] = "(^|\W|\s+){$nom}(\s+)?(ribu|rb|juta|jt)"; // price

		foreach ( $paterns as $val ) {
			if( preg_match("/" . $val . "/i", $str, $match) ) {
				fecho("[!] Blacklisted : (price) Contain {$match[0]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
				return true;
			}
		}
		
		return false;
	}
	
	function contains_sosmed($str){
		$paterns[] = "(^|\/| )(facebook|fb|twitter|insta( ?gram)?|ig|(chat\s+)?(id([\s\.-_])?)?line[^\w](chat)?((\s+)?id)?)([\s\.:=\@]+)?[a-z0-9_]{3,}"; // sosmed
		$paterns[] = "(^|\/| )(pin((\s+)?bbm?)?|bbm?((\s+)?pin)?)([\s:]+)?[a-f0-9]{8}"; // sosmed
		$paterns[] = "(^|\/| )[a-f0-9]{8}([\s:]+)?(\(?(pin((\s+)?bbm?)?|bbm?((\s+)?pin)?)\)?)"; // sosmed
		$paterns[] = "(^| )(bb ?channel)"; // sosmed


		
		foreach ( $paterns as $val ) {
			if( preg_match("/{$val}/i", $str, $match) ) {
				fecho("[!] Blacklisted : (sosmed) Contain {$match[0]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
				return true;
			}
		}
		
		$patern = "(^|\/| )(pin((\s+)?bbm?)?|bbm?((\s+)?pin)?)([\s:]+)?([a-f0-9_\-.\s]{8,})"; // sosmed
		if( preg_match("/{$patern}/i", $str, $match) ) {
			if(isset($match[8])){
				$pin = preg_replace('/[^a-f0-9]/i', '', $match[8]);
				if ( strlen($pin) == 8 ) {
					fecho("[!] Blacklisted : (sosmed) Contain {$match[8]} -> {$str}");
					write_log("blacklist_match.txt", "{$val} -> [{$match[8]}] -> {$str}");
					return true;
				}
			}

		}
		
		return false;
	}
	
	function contains_junk($str){
		$paterns[] = "(^| )(cod[^\w]( ?area ?:?)?|(quick|fast) respons?)"; // contact
		$paterns[] = "(^| )\#[a-z0-9]{2,} "; // hastag
		$paterns[] = "(^| )(showroom|reseller|store|toko( ?o?nline)?|dropship|itc( ?roxymas)? |mangga( +)?(dua|2)|alamat)"; // address
		$paterns[] = "(^| )((info ?selengkapnya )?(dapat|bisa) (langsung )?menghubungi|(silahkan )?(kunjungi|berkunjung)( ke)?|info lebih lanjut hubungi|admin)"; // address
		$paterns[] = "(^| )(ja?la?n?|perumahan|perum|komplek|komp)[\s\.]"; // address
		$paterns[] = "(^| )(lantai|lt)[\s\.]\d+"; // address
		$paterns[] = "(^|\W|\s+)j\&t|jne|tiki|go([\s\._-])?(jek|jeg|send|kilat)|grab"; // expedition
		$paterns[] = "(^| )(perhatian|cek stock barang|tidak setuju|jangan beli|bayar ditempat|(testimoni(al)?)|pemesanan)"; // junk
		$paterns[] = "(^| )(ready|terjual)(.*?)\d+ ?pcs"; // junk
		$paterns[] = "([^\d]+)?\@([a-z0-9-_]{3,})"; // @
		$paterns[] = "^((\s+)?[\#\*\=\-\.\:\<\>]{3,}).+"; // rp
		$paterns[] = "Owner|gradeorijersey|kico|pratwonamshop|xoxo|raymond yauw s\.ars"; // junk
		$paterns[] = "(^| )(bergerak di(\s+)?bidang|toko sebelah|harap bilang|kebijakan toko|melayani pesanan|dapatkan|diskon|bonus|hadiah|free)"; // junk
		$paterns[] = "(partai besar|grosir|asli ?\!{1,}|awas( ?\!{1,})?|promo (masih )?berlaku)|belum termasuk (ongkir|ongkos kirim)"; // junk
		$paterns[] = "shop|google|yahoo|hotmail|e?mail|\w+store|bbm[\s:;]+"; 
		$paterns[] = "whatsapp|whatsup|wasap|\+62[\d\s._-]+"; 
		$paterns[] = "jabode|jakarta|jkt (pusat|utara|selatan|barat|timur)|bogor|depok|tangerang|bekasi"; // address


		
		foreach ( $paterns as $val ) {
			if( preg_match("/{$val}/i", $str, $match) ) {
				fecho("[!] Blacklisted : (junk) Contain {$match[0]} -> {$str}");
				write_log("blacklist_match.txt", "{$val} -> [{$match[0]}] -> {$str}");
				return true;
			}
		}
		
		return false;
	}
	
	function make_clean($content){
		if( is_array($content) ){
			$result = array();
			foreach($content as $k => $v){
				if(is_array($v)){
					$result[$k] = make_clean($v);
				}else{
					$v = preg_replace('/\?trkid=(.*)/i','',$v);
					$v = preg_replace('/(bisa|khusus( +)?)?(via( +)?)?(gojek|gosend|gokilat|grab)(( +)?ready)?/i','',$v);
					$v = preg_replace('/wholeseller/i','',$v);
					$v = preg_replace('/sku([\s:]+)?\d+/i','',$v);
					$v = preg_replace('/[\W\s]+$/i','',$v);
					$v = preg_replace('/\((\W+)?\)|\{(\W+)?\}|\[(\W+)?\]/i','',$v);
					$result[$k] = clean_unicode($v);
				}
			}
			return $result;
		}else{
			return clean_unicode($content);
		}
	}
	
	function clean_unicode($str){
		$str = preg_replace('/[\x80-\xff]/','',$str);
		$str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
		}, $str);

		return $str;
	}
	
	function code_area(){
		// Aceh
		$code_area['0627'] = 'Kota Subulussalam';
		$code_area['0629'] = 'Kutacane (Kabupaten Aceh Tenggara)';
		$code_area['0641'] = 'Kota Langsa';
		$code_area['0642'] = 'Blang Kejeren (Kabupaten Gayo Lues)';
		$code_area['0643'] = 'Takengon (Kabupaten Aceh Tengah)';
		$code_area['0644'] = 'Bireuen (Kabupaten Bireuen)';
		$code_area['0645'] = 'Lhoksukon (Kabupaten Aceh Utara) - Kota Lhokseumawe';
		$code_area['0646'] = 'Idi (Kabupaten Aceh Timur)';
		$code_area['0650'] = 'Sinabang (Kabupaten Simeulue)';
		$code_area['0651'] = 'Kota Banda Aceh - Jantho (Kabupaten Aceh Besar) - Lamno (Kabupaten Aceh Jaya)';
		$code_area['0652'] = 'Kota Sabang';
		$code_area['0653'] = 'Sigli (Kabupaten Pidie)';
		$code_area['0654'] = 'Calang (Kabupaten Aceh Jaya)';
		$code_area['0655'] = 'Meulaboh (Kabupaten Aceh Barat)';
		$code_area['0656'] = 'Tapaktuan (Kabupaten Aceh Selatan)';
		$code_area['0657'] = 'Bakongan (Kabupaten Aceh Selatan)';
		$code_area['0658'] = 'Singkil (Kabupaten Aceh Singkil)';
		$code_area['0659'] = 'Blangpidie (Kabupaten Aceh Barat Daya)';

		//Sumatera Utara
		$code_area['061'] = 'Kota Medan - Kota Binjai - Stabat (Kabupaten Langkat) - Lubuk Pakam (Kabupaten Deli Serdang) - Perbaungan - Pantai Cermin (Kabupaten Serdang Bedagai)';
		$code_area['0620'] = 'Pangkalan Brandan (Kabupaten Langkat)';
		$code_area['0621'] = 'Kota Tebing Tinggi - Sei Rampah (Kabupaten Serdang Bedagai)';
		$code_area['0622'] = 'Kota Pematangsiantar - Pematang Raya (Kabupaten Simalungun) - Limapuluh (Kabupaten Batu Bara)';
		$code_area['0623'] = 'Kisaran (Kabupaten Asahan) - Kota Tanjung Balai';
		$code_area['0624'] = 'Rantau Prapat (Kabupaten Labuhanbatu) - Aek Kanopan (Kabupaten Labuhanbatu Utara) - Kota Pinang (Kabupaten Labuhanbatu Selatan)';
		$code_area['0625'] = 'Parapat (Kabupaten Simalungun) - Ajibata (Kabupaten Toba Samosir) - Simanindo (Kabupaten Samosir)';
		$code_area['0626'] = 'Pangururan (Kabupaten Samosir)';
		$code_area['0627'] = 'Sidikalang (Kabupaten Dairi) - Salak (Kabupaten Pakpak Bharat)';
		$code_area['0628'] = 'Kabanjahe (Kabupaten Karo) - Sibolangit (Kabupaten Deli Serdang)';
		$code_area['0630'] = 'Teluk Dalam (Kabupaten Nias Selatan)';
		$code_area['0631'] = 'Kota Sibolga - Pandan (Kabupaten Tapanuli Tengah)';
		$code_area['0632'] = 'Balige (Kabupaten Toba Samosir)';
		$code_area['0633'] = 'Tarutung (Kabupaten Tapanuli Utara) - Dolok Sanggul (Kabupaten Humbang Hasundutan)';
		$code_area['0634'] = 'Kota Padang Sidempuan - Sipirok (Kabupaten Tapanuli Selatan)';
		$code_area['0635'] = 'Gunung Tua (Kabupaten Padang Lawas Utara)';
		$code_area['0636'] = 'Panyabungan (Kabupaten Mandailing Natal) - Sibuhuan (Kabupaten Padang Lawas)';
		$code_area['0639'] = 'Kota Gunung Sitoli';

		//Sumatera Barat
		$code_area['0751'] = 'Kota Padang - Kota Pariaman - Kabupaten Padang Pariaman';
		$code_area['0752'] = 'Kota Bukittinggi - Kota Padang Panjang - Kota Payakumbuh - Kabupaten Tanah Datar - Kabupaten Agam';
		$code_area['0753'] = 'Kabupaten Pasaman - Kabupaten Pasaman Barat';
		$code_area['0754'] = 'Kota Sawahlunto - Kabupaten Sijunjung - Kabupaten Dharmasraya';
		$code_area['0755'] = 'Kota Solok - Kabupaten Solok Selatan - Arosuka (Kabupaten Solok)';
		$code_area['0756'] = 'Painan (Kabupaten Pesisir Selatan)';
		$code_area['0757'] = 'Renah Indojati';
		$code_area['0759'] = 'Kabupaten Kepulauan Mentawai';

		//Riau
		$code_area['0760'] = 'Teluk Kuantan (Kabupaten Kuantan Singingi)';
		$code_area['0761'] = 'Kota Pekanbaru - Pangkalan Kerinci (Kabupaten Pelalawan) - Minas - Tualang (Kabupaten Siak)';
		$code_area['0762'] = 'Bangkinang (Kabupaten Kampar) - Pasir Pengaraian (Kabupaten Rokan Hulu)';
		$code_area['0763'] = 'Selatpanjang (Kabupaten Kepulauan Meranti)';
		$code_area['0764'] = 'Siak Sri Indrapura (Kabupaten Siak)';
		$code_area['0765'] = 'Kota Dumai - Duri (Kabupaten Bengkalis) - Bagan Batu (Kabupaten Rokan Hilir) - Ujung Tanjung (Kabupaten Rokan Hilir)';
		$code_area['0766'] = 'Bengkalis (Kabupaten Bengkalis)';
		$code_area['0767'] = 'Bagansiapiapi (Kabupaten Rokan Hilir)';
		$code_area['0768'] = 'Tembilahan (Kabupaten Indragiri Hilir)';
		$code_area['0769'] = 'Rengat - Air Molek (Kabupaten Indragiri Hulu)';
		$code_area['0624'] = 'Panipahan (Kabupaten Rokan Hilir)';

		//Kepulauan Riau
		$code_area['0770'] = 'Batam Industrial Zone (Mukakuning)';
		$code_area['0771'] = 'Kota Tanjungpinang dan Kabupaten Bintan';
		$code_area['0772'] = 'Tarempa (Kabupaten Kepulauan Anambas)';
		$code_area['0773'] = 'Ranai (Kabupaten Natuna)';
		$code_area['0776'] = 'Dabosingkep (Kabupaten Lingga)';
		$code_area['0777'] = 'Tanjung Balai Karimun (Kabupaten Karimun)';
		$code_area['0778'] = 'Kota Batam';
		$code_area['0779'] = 'Tanjungbatu (Kabupaten Karimun)';

		//Jambi
		$code_area['0740'] = 'Mendahara - Muara Sabak (Kabupaten Tanjung Jabung Timur)';
		$code_area['0741'] = 'Kota Jambi';
		$code_area['0742'] = 'Kualatungkal (Kabupaten Tanjung Jabung Barat) - Tebingtinggijambi';
		$code_area['0743'] = 'Muara Bulian (Kabupaten Batanghari)';
		$code_area['0744'] = 'Muara Tebo (Kabupaten Tebo)';
		$code_area['0745'] = 'Sarolangun (Kabupaten Sarolangun)';
		$code_area['0746'] = 'Bangko (Kabupaten Merangin)';
		$code_area['0747'] = 'Muarabungo (Kabupaten Bungo)';
		$code_area['0748'] = 'Kota Sungai Penuh dan Kabupaten Kerinci';

		//Sumatera Selatan
		$code_area['0702'] = 'Tebing Tinggi (Kabupaten Empat Lawang)';
		$code_area['0711'] = 'Kota Palembang - Pangkalan Balai - Betung (Kabupaten Banyuasin) - Indralaya (Kabupaten Ogan Ilir)';
		$code_area['0712'] = 'Kayu Agung (Kabupaten Ogan Komering Ilir) - Tanjung Raja (Kabupaten Ogan Ilir)';
		$code_area['0713'] = 'Kota Prabumulih - Pendopo Talang Ubi (Kabupaten Muara Enim)';
		$code_area['0714'] = 'Sekayu (Kabupaten Musi Banyuasin)';
		$code_area['0730'] = 'Kota Pagar Alam - Kota Agung (Kabupaten Lahat)';
		$code_area['0731'] = 'Lahat (Kabupaten Lahat)';
		$code_area['0733'] = 'Kota Lubuklinggau - Muara Beliti (Kabupaten Musi Rawas)';
		$code_area['0734'] = 'Muara Enim (Kabupaten Muara Enim)';
		$code_area['0735'] = 'Baturaja (Kabupaten Ogan Komering Ulu) - Martapura (Kabupaten Ogan Komering Ulu Timur) - Muaradua (Kabupaten Ogan Komering Ulu Selatan)';

		//Kepulauan Bangka Belitung
		$code_area['0715'] = 'Belinyu (Kabupaten Bangka)';
		$code_area['0716'] = 'Muntok (Kabupaten Bangka Barat)';
		$code_area['0717'] = 'Kota Pangkal Pinang - Sungailiat (Kabupaten Bangka)';
		$code_area['0718'] = 'Koba (Kabupaten Bangka Tengah) - Toboali (Kabupaten Bangka Selatan)';
		$code_area['0719'] = 'Manggar (Kabupaten Belitung Timur) - Tanjung Pandan (Kabupaten Belitung)';

		//Bengkulu
		$code_area['0732'] = 'Curup (Kabupaten Rejang Lebong)';
		$code_area['0736'] = 'Kota Bengkulu - Lais (Kabupaten Bengkulu Utara)';
		$code_area['0737'] = 'Arga Makmur (Kabupaten Bengkulu Utara) - Mukomuko (Kabupaten Mukomuko)';
		$code_area['0738'] = 'Muara Aman (Kabupaten Lebong)';
		$code_area['0739'] = 'Bintuhan (Kabupaten Kaur) - Kota Manna (Kabupaten Bengkulu Selatan)';

		//Lampung
		$code_area['0721'] = 'Kota Bandar Lampung - Gedong Tataan - Tegineneng (Kabupaten Pesawaran) - Natar - Jati Agung (Kabupaten Lampung Selatan)';
		$code_area['0722'] = 'Kota Agung (Kabupaten Tanggamus)';
		$code_area['0723'] = 'Blambangan Umpu (Kabupaten Way Kanan)';
		$code_area['0724'] = 'Kotabumi (Kabupaten Lampung Utara)';
		$code_area['0725'] = 'Kota Metro - Gunung Sugih (Kabupaten Lampung Tengah) - Sukadana (Kabupaten Lampung Timur)';
		$code_area['0726'] = 'Menggala (Kabupaten Tulang Bawang) - Kabupaten Tulang Bawang Barat - Wiralaga Mulya (Kabupaten Mesuji)';
		$code_area['0727'] = 'Kalianda (Kabupaten Lampung Selatan) - Punduh Pidada (Kabupaten Pesawaran)';
		$code_area['0728'] = 'Kota Liwa (Kabupaten Lampung Barat) - Krui (Kabupaten Pesisir Barat)';
		$code_area['0729'] = 'Pringsewu (Kabupaten Pringsewu)';

		//Jakarta
		$code_area['021'] = 'Kepulauan Seribu - Jakarta Barat - Jakarta Pusat - Jakarta Selatan - Jakarta Timur - Jakarta Utara';

		//Banten
		$code_area['021'] = 'Tigaraksa (Kabupaten Tangerang) - Kota Tangerang - Kota Tangerang Selatan (Pamulang)';
		$code_area['0252'] = 'Rangkasbitung (Kabupaten Lebak)';
		$code_area['0253'] = 'Pandeglang - Labuan (Kabupaten Pandeglang)';
		$code_area['0254'] = 'Kota Serang - Kabupaten Serang - Merak (Kota Cilegon)';
		$code_area['0257'] = 'Cinangka (Kabupaten Serang)';

		//Jawa Barat
		$code_area['021'] = 'DKI Jakarta - Kota Bekasi - Kabupaten Bekasi - Kota Depok kecuali Sawangan dan Bojongsari - Cibinong - Cileungsi - Klapanunggal - Jonggol - Sukamakmur - Cariu - Tanjungsari (Kabupaten Bogor)';
		$code_area['022'] = 'Kota Bandung - Kota Cimahi - Soreang (Kabupaten Bandung) - Lembang - Ngamprah (Kabupaten Bandung Barat)';
		$code_area['0231'] = 'Kota Cirebon - Sumber - Losari (Kabupaten Cirebon)';
		$code_area['0232'] = 'Kabupaten Kuningan';
		$code_area['0233'] = 'Kabupaten Majalengka';
		$code_area['0234'] = 'Kabupaten Indramayu';
		$code_area['0251'] = 'Kota Bogor - Sawangan - Bojongsari (Kota Depok) - Kabupaten Bogor kecuali Cibinong, Cileungsi, Klapanunggal, Jonggol, Sukamakmur, Cariu, dan Tanjungsari';
		$code_area['0260'] = 'Kabupaten Subang';
		$code_area['0261'] = 'Kabupaten Sumedang';
		$code_area['0262'] = 'Kabupaten Garut';
		$code_area['0263'] = 'Kabupaten Cianjur';
		$code_area['0264'] = 'Kabupaten Purwakarta - Cikampek (Kabupaten Karawang)';
		$code_area['0265'] = 'Kota Tasikmalaya - Kadipaten - Singaparna (Kabupaten Tasikmalaya) - Kota Banjar - Kabupaten Ciamis - Kabupaten Pangandaran';
		$code_area['0266'] = 'Kota Sukabumi - Palabuhanratu (Kabupaten Sukabumi) - Gekbrong (Kabupaten Cianjur)';
		$code_area['0267'] = 'Kabupaten Karawang';

		//Jawa Tengah
		$code_area['024'] = 'Semarang, Ungaran, Demak (Mranggen, Sayung)';
		$code_area['0271'] = 'Surakarta (Solo), Kartasura, Sukoharjo, Karanganyar, Sragen, sebagian Boyolali';
		$code_area['0272'] = 'Klaten';
		$code_area['0273'] = 'Wonogiri, Purwantoro, Pracimantoro';
		$code_area['0274'] = 'Prambanan, Klaten (wilayah yang berbatasan dengan Yogyakarta)';
		$code_area['0275'] = 'Purworejo, Kutoarjo';
		$code_area['0276'] = 'Boyolali';
		$code_area['0280'] = 'Majenang, Sidareja (Kabupaten Cilacap bagian barat)';
		$code_area['0281'] = 'Purwokerto, Banyumas, Sumpiuh, Purbalingga';
		$code_area['0282'] = 'Cilacap (bagian timur), Kebasen (wilayah yang berbatasan dengan Cilacap)';
		$code_area['0283'] = 'Tegal, Slawi, Brebes (kecuali Bumiayu)';
		$code_area['0284'] = 'Pemalang, Tegal (bagian timur)';
		$code_area['0285'] = 'Pekalongan, Batang (bagian barat), Comal';
		$code_area['0286'] = 'Banjarnegara, Wonosobo';
		$code_area['0287'] = 'Kebumen, Karanganyar, Gombong, Tambak (wilayah yang berbatasan dengan Kebumen)';
		$code_area['0289'] = 'Bumiayu, Paguyangan (Kabupaten Brebes bagian selatan)';
		$code_area['0291'] = 'Demak (kecuali Mranggen, Sayung), Jepara, Kudus';
		$code_area['0292'] = 'Grobogan, Purwodadi';
		$code_area['0293'] = 'Magelang, Mungkid, Temanggung';
		$code_area['0294'] = 'Kendal, Kaliwungu, Weleri, Batang (bagian timur)';
		$code_area['0295'] = 'Pati, Rembang, Lasem';
		$code_area['0296'] = 'Blora, Cepu';
		$code_area['0297'] = 'Karimun Jawa';
		$code_area['0298'] = 'Salatiga, Ambarawa (Kabupaten Semarang bagian tengah dan selatan)';
		$code_area['0299'] = 'Nusakambangan';
		$code_area['0356'] = 'Rembang bagian Timur (wilayah yang berbatasan dengan Tuban)';

		//Yogyakarta
		$code_area['0274'] = 'Kota Yogyakarta, Sleman, Bantul, Gunung Kidul, Kulon Progo';

		//Jawa Timur
		$code_area['031'] = 'Surabaya, Gresik, Sidoarjo, Bangkalan';
		$code_area['0321'] = 'Mojokerto, Jombang';
		$code_area['0322'] = 'Lamongan, Babat';
		$code_area['0323'] = 'Sampang';
		$code_area['0324'] = 'Pamekasan';
		$code_area['0325'] = 'Sangkapura (Bawean)';
		$code_area['0327'] = 'Kepulauan Kangean, Kepulauan Masalembu';
		$code_area['0328'] = 'Sumenep';
		$code_area['0331'] = 'Jember (kecuali Ambulu dan Puger)';
		$code_area['0332'] = 'Bondowoso, Sukosari, Prajekan';
		$code_area['0333'] = 'Banyuwangi, Genteng, Muncar';
		$code_area['0334'] = 'Lumajang';
		$code_area['0335'] = 'Probolinggo, Kraksaan';
		$code_area['0336'] = 'Ambulu, Puger (Kabupaten Jember bagian selatan)';
		$code_area['0338'] = 'Situbondo, Besuki';
		$code_area['0341'] = 'Malang, Kepanjen, Batu';
		$code_area['0342'] = 'Blitar, Wlingi';
		$code_area['0343'] = 'Pasuruan, Pandaan, Gempol';
		$code_area['0351'] = 'Madiun, Caruban, Magetan, Ngawi';
		$code_area['0352'] = 'Ponorogo';
		$code_area['0353'] = 'Bojonegoro';
		$code_area['0354'] = 'Kediri, Pare';
		$code_area['0355'] = 'Tulungagung, Trenggalek';
		$code_area['0356'] = 'Tuban';
		$code_area['0357'] = 'Pacitan';
		$code_area['0358'] = 'Nganjuk, Kertosono';

		//Bali
		$code_area['0361'] = 'Denpasar, Gianyar, Badung, Tabanan (kecuali Baturiti), Tampaksiring, Ubud (Gianyar), Nusa Dua, Kuta (Badung), Sanur (Denpasar Selatan)';
		$code_area['0362'] = 'Singaraja (Kabupaten Buleleng)';
		$code_area['0363'] = 'Amlapura (Kabupaten Karangasem)';
		$code_area['0365'] = 'Negara, Gilimanuk (Kabupaten Jembrana)';
		$code_area['0366'] = 'Klungkung, Bangli';
		$code_area['0368'] = 'Baturiti (Tabanan)';

		//Nusa Tenggara Barat
		$code_area['0364'] = 'Kota Mataram';
		$code_area['0370'] = 'Mataram, Praya';
		$code_area['0371'] = 'Sumbawa';
		$code_area['0372'] = 'Alas, Taliwang';
		$code_area['0373'] = 'Dompu';
		$code_area['0374'] = 'Bima';
		$code_area['0376'] = 'Selong';

		//Nusa Tenggara Timur
		$code_area['0380'] = 'Kupang, Baa (Roti)';
		$code_area['0381'] = 'Ende';
		$code_area['0382'] = 'Maumere';
		$code_area['0383'] = 'Larantuka';
		$code_area['0384'] = 'Bajawa';
		$code_area['0385'] = 'Labuhanbajo, Ruteng';
		$code_area['0386'] = 'Kalabahi';
		$code_area['0387'] = 'Waingapu, Waikabubak';
		$code_area['0388'] = 'Kefamenanu, Soe';
		$code_area['0389'] = 'Atambua';

		//Kalimantan Barat
		$code_area['0561'] = 'Pontianak, Mempawah';
		$code_area['0562'] = 'Sambas, Singkawang, Bengkayang';
		$code_area['0563'] = 'Ngabang';
		$code_area['0564'] = 'Sanggau';
		$code_area['0565'] = 'Sintang';
		$code_area['0567'] = 'Putussibau';
		$code_area['0568'] = 'Nanga Pinoh';
		$code_area['0534'] = 'Ketapang';

		//Kalimantan Tengah
		$code_area['0513'] = 'Muara Teweh';
		$code_area['0522'] = 'Ampah (Dusun Tengah, Barito Timur)';
		$code_area['0525'] = 'Buntok';
		$code_area['0526'] = 'Tamiang Layang';
		$code_area['0528'] = 'Purukcahu';
		$code_area['0531'] = 'Sampit';
		$code_area['0532'] = 'Pangkalan Bun, Kumai';
		$code_area['0536'] = 'Palangkaraya, Kasongan';
		$code_area['0537'] = 'Kuala Kurun';
		$code_area['0538'] = 'Kuala Pembuang';
		$code_area['0539'] = 'Kuala Kuayan (Mentaya Hulu, Kotawaringin Timur)';

		//Kalimantan Selatan
		$code_area['0511'] = 'Banjarmasin, Banjarbaru, Martapura, Marabahan';
		$code_area['0512'] = 'Pelaihari';
		$code_area['0517'] = 'Kandangan, Barabai, Rantau, Negara';
		$code_area['0518'] = 'Kotabaru, Batulicin';
		$code_area['0526'] = 'Tanjung, Balangan';
		$code_area['0527'] = 'Amuntai';

		//Kalimantan Timur
		$code_area['0541'] = 'Samarinda, Tenggarong';
		$code_area['0542'] = 'Balikpapan';
		$code_area['0543'] = 'Tanah Grogot';
		$code_area['0545'] = 'Melak';
		$code_area['0548'] = 'Bontang';
		$code_area['0549'] = 'Sangatta';
		$code_area['0554'] = 'Tanjung Redeb (Berau)';

		//Kalimantan Utara
		$code_area['0551'] = 'Tarakan, Bunyu';
		$code_area['0552'] = 'Tanjungselor, Tana Tidung';
		$code_area['0553'] = 'Malinau';
		$code_area['0556'] = 'Nunukan';

		//Sulawesi Utara
		$code_area['0430'] = 'Amurang (Kabupaten Minahasa Selatan)';
		$code_area['0431'] = 'Manado, Tomohon, Tondano';
		$code_area['0432'] = 'Tahuna (Kabupaten Kepulauan Sangihe)';
		$code_area['0434'] = 'Kotamobagu';
		$code_area['0438'] = 'Bitung';

		//Gorontalo
		$code_area['0435'] = 'Gorontalo, Limboto';
		$code_area['0443'] = 'Marisa';

		//Sulawesi Tengah
		$code_area['0445'] = 'Buol';
		$code_area['0450'] = 'Parigi';
		$code_area['0451'] = 'Palu';
		$code_area['0452'] = 'Poso';
		$code_area['0453'] = 'Tolitoli';
		$code_area['0454'] = 'Tinombo';
		$code_area['0457'] = 'Donggala';
		$code_area['0458'] = 'Tentena';
		$code_area['0461'] = 'Luwuk';
		$code_area['0462'] = 'Banggai';
		$code_area['0463'] = 'Bunta';
		$code_area['0464'] = 'Ampana';
		$code_area['0465'] = 'Kolonedale';
		$code_area['0455'] = 'kotaraya,moutong';

		//Sulawesi Barat
		$code_area['0422'] = 'Majene';
		$code_area['0426'] = 'Mamuju';
		$code_area['0428'] = 'Polewali';

		//Sulawesi Selatan
		$code_area['0410'] = 'Pangkep';
		$code_area['0411'] = 'Makassar, Maros, Sungguminasa';
		$code_area['0413'] = 'Bulukumba, Bantaeng';
		$code_area['0414'] = 'Kepulauan Selayar';
		$code_area['0417'] = 'Malino';
		$code_area['0418'] = 'Takalar';
		$code_area['0419'] = 'Jeneponto';
		$code_area['0420'] = 'Enrekang';
		$code_area['0421'] = 'Parepare, Pinrang';
		$code_area['0423'] = 'Makale, Rantepao';
		$code_area['0427'] = 'Barru';
		$code_area['0471'] = 'Palopo';
		$code_area['0472'] = 'Pitumpanua';
		$code_area['0473'] = 'Masamba';
		$code_area['0474'] = 'Malili';
		$code_area['0475'] = 'Soroako';
		$code_area['0481'] = 'Watampone';
		$code_area['0482'] = 'Sinjai';
		$code_area['0484'] = 'Watansoppeng';
		$code_area['0485'] = 'Sengkang';

		//Sulawesi Tenggara
		$code_area['0401'] = 'Kendari';
		$code_area['0402'] = 'Baubau';
		$code_area['0403'] = 'Raha';
		$code_area['0404'] = 'Wanci';
		$code_area['0405'] = 'Kolaka';
		$code_area['0408'] = 'Kota Unaaha';

		//Maluku
		$code_area['0910'] = 'Bandanaira';
		$code_area['0911'] = 'Ambon';
		$code_area['0913'] = 'Namlea';
		$code_area['0914'] = 'Masohi';
		$code_area['0915'] = 'Bula';
		$code_area['0916'] = 'Tual';
		$code_area['0917'] = 'Dobo';
		$code_area['0918'] = 'Saumlaku';
		$code_area['0921'] = 'Soasiu, Sofifi';
		$code_area['0922'] = 'Jailolo';
		$code_area['0923'] = 'Morotai';
		$code_area['0924'] = 'Tobelo';
		$code_area['0927'] = 'Labuha';
		$code_area['0929'] = 'Sanana';
		$code_area['0931'] = 'Saparua';


		//Papua
		$code_area['0901'] = 'Timika, Tembagapura';
		$code_area['0902'] = 'Agats (Asmat)';
		$code_area['0951'] = 'Sorong';
		$code_area['0952'] = 'Teminabuan';
		$code_area['0955'] = 'Bintuni';
		$code_area['0956'] = 'Fakfak';
		$code_area['0957'] = 'Kaimana';
		$code_area['0966'] = 'Sarmi';
		$code_area['0967'] = 'Jayapura, Abepura';
		$code_area['0969'] = 'Wamena';
		$code_area['0971'] = 'Merauke';
		$code_area['0975'] = 'Tanahmerah';
		$code_area['0980'] = 'Ransiki';
		$code_area['0981'] = 'Biak';
		$code_area['0983'] = 'Serui';
		$code_area['0984'] = 'Nabire';
		$code_area['0985'] = 'Nabire';
		$code_area['0986'] = 'Manokwari';
		return $code_area;
	}

	function code_mobile(){
		//Telkomsel
		$code_mobile['0811'] = 'Telkomsel';
		$code_mobile['0812'] = 'Telkomsel';
		$code_mobile['0813'] = 'Telkomsel';
		$code_mobile['0851'] = 'Telkomsel'; //(Flexi)
		$code_mobile['0852'] = 'Telkomsel';
		$code_mobile['0853'] = 'Telkomsel';
		$code_mobile['0821'] = 'Telkomsel';
		$code_mobile['0822'] = 'Telkomsel';
		$code_mobile['0823'] = 'Telkomsel';

		//Indosat Ooredoo
		$code_mobile['0814'] = 'Indosat Ooredoo';
		$code_mobile['0815'] = 'Indosat Ooredoo';
		$code_mobile['0816'] = 'Indosat Ooredoo';
		$code_mobile['0855'] = 'Indosat Ooredoo';
		$code_mobile['0856'] = 'Indosat Ooredoo';
		$code_mobile['0857'] = 'Indosat Ooredoo';
		$code_mobile['0858'] = 'Indosat Ooredoo';

		//XL Axiata
		$code_mobile['0817'] = 'XL Axiata';
		$code_mobile['0818'] = 'XL Axiata';
		$code_mobile['0819'] = 'XL Axiata';
		$code_mobile['0859'] = 'XL Axiata';
		$code_mobile['0877'] = 'XL Axiata';
		$code_mobile['0878'] = 'XL Axiata';

		//AXIS (XL)
		$code_mobile['0831'] = 'AXIS (XL)';
		$code_mobile['0832'] = 'AXIS (XL)';
		$code_mobile['0833'] = 'AXIS (XL)';
		$code_mobile['0838'] = 'AXIS (XL)';

		//Sampoerna Telekomunikasi Indonesia (Net1 Indonesia)
		$code_mobile['0827'] = 'Net1 Indonesia';
		$code_mobile['0828'] = 'Net1 Indonesia';

		//Smartfren
		$code_mobile['0881'] = 'Smartfren';
		$code_mobile['0882'] = 'Smartfren';
		$code_mobile['0883'] = 'Smartfren';
		$code_mobile['0884'] = 'Smartfren';
		$code_mobile['0885'] = 'Smartfren';
		$code_mobile['0886'] = 'Smartfren';
		$code_mobile['0887'] = 'Smartfren';
		$code_mobile['0888'] = 'Smartfren';
		$code_mobile['0889'] = 'Smartfren';

		// 3 (Tri)
		$code_mobile['0895'] = 'Tri';
		$code_mobile['0896'] = 'Tri';
		$code_mobile['0897'] = 'Tri';
		$code_mobile['0898'] = 'Tri';
		$code_mobile['0899'] = 'Tri';
		return $code_mobile;
	}
	
	/* End Function */

	if(! empty($argv[1])){
		$file = $argv[1];
	}else{
		myheader($argv[0]);
		exit();
	}
	
	$row 	 = 1;
	$limit	 = 100000;
	$delete  = array();
	$handle  = fopen($file,"r");
	
	if (! file_exists('backup')) {
		fecho("[*] Create backup folder...");
		mkdir('backup', 0777, true);
	}
	
	if (! file_exists('backup/'.$file)) {
		fecho("[*] Create backup file...");
		copy($file, 'backup/'.$file);
	}
	
	echo "[*] Start remove duplicate product...\n";
	
	while(! feof($handle) && $row <= $limit) {
		$data[] = fgetcsv($handle);
		$row++;
	}
	fclose($handle);
	
	$data = array_filter(make_clean($data));
	$debug_arr = array();
	
	$bcount = count($data);
	if(!$data) exit;
	
	$mcount = count($data[0]);
	
	foreach($data as $key => $sample) {
		if(count($sample) != $mcount){
			unset($data[$key]);
			continue;
		}
		$ident = make_key($sample[1]);
		$debug_arr[$key] = $sample;
		if(in_array($ident, $delete)) {
			$fkey = array_search($ident, $delete);
			//fecho("----- MATCH -----");
			//fecho("[+] URL1 => {$sample[0]}");
			//fecho("[+] URL2 => {$debug_arr[$fkey][0]}");
			//fecho("[+] ident[{$fkey}] => {$ident}");
			//fecho("[+] data[{$fkey}] => {$debug_arr[$fkey][1]}");
			//fecho("[+] match[{$key}] => {$sample[1]}");
			//fecho("-----------------\n");
			unset($data[$key]);
			continue;
		}
		$delete[$key] = $ident;
	}
	
	$acount = count(array_filter($data));

	fecho("[*] before clean data : {$bcount}");
	fecho("[*] after  clean data : {$acount}\n");
	fecho("[*] Try to clean blacklist keyword\n");
	sleep(5);

	$fp = fopen($file, 'w');
	foreach ($data as $key=>$value) {
		if( $key !=0 && isset($value[13])){

			$fields  = array();
			$arr_des = array_values(array_map('trim', explode("\n", $value[13])));
			foreach($arr_des as $des){
			    //contains_price($des);
				$des = html_entity_decode($des);

			    if( filter_blacklist($des) ){
					$fields[] = $des;
				}
			}
			
			$value[13] = implode("\n", $fields);
		}
		
		if(is_array($value)) {
			fputcsv($fp, $value);
		}
	}
	fclose($fp);
	
	
	echo "DONE!";
	
