import os
import sys
import random
import time
import datetime

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_all_elements_located, \
    staleness_of
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import WebDriverException

#LIST_ACCOUNT = sys.argv[1]
LIST_ACCOUNT = 'akun.txt'
CHROME_DRIVER = 'chromedriver.exe'
#CHROME_DRIVER = 'QloBOT\driver\chromedriver.exe'
LOGIN_URL = 'https://seller.shopee.co.id'
FIRST_PAGE_URL = 'https://seller.shopee.co.id/portal/product/list/active?page=1'
PAGE_LOAD_TIMEOUT = 60
MAX_CLICKS = 5
ELEM_TO_CLICK_TEXT_ID = 'Naikkan Produk'
ELEM_TO_CLICK_TEXT_EN = 'Boost Now'
TIME_TO_SLEEP = 5 * 60

SELECTOR_USERNAME = '[placeholder*="Email/Telepon/Username"]'
SELECTOR_PASSWORD = '[placeholder*="Password"]'
#SELECTOR_LOGIN = '.shopee-validation-manager .shopee-button'
SELECTOR_LOGIN = 'button.shopee-button.shopee-button--primary.shopee-button--block'
SELECTOR_LOGGED_IN = '.account-header-dropdown__name'
SELECTOR_PAGINATOR_ELEMS = '.shopee-pagination--footer li'
SELECTOR_GO_TO_PAGE = '.ember-text-field[type="tel"]'
SELECTOR_PRODUCTS = '.product-items__item'
SELECTOR_ELEMS_TO_CLICK = '.product-boost-button .shopee-button'


def timenow():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')

def datetimenow():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    
def next_time(tambah):
    return datetime.datetime.fromtimestamp(time.time() + tambah).strftime('%H:%M:%S')

def format_time(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d jam %02d menit %02d detik" % (h, m, s)

def can_exe(username):
    directory = 'cache'
    exe = True
    file = directory + '/' + username + '.dat'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if os.path.exists(file):
        if (time.time() - os.path.getmtime(file)) < 14400:
            lagi = os.path.getmtime(file) + 14400
            print ('[+] '+ timenow() +' User '+ username +' akan nyundul lagi pada... ' + datetime.datetime.fromtimestamp(lagi).strftime('%Y-%m-%d %H:%M:%S'))
            exe = False
    
    return exe
        
def sukses(data):
    file = 'sukses.txt'
    f=open(file,'a')
    f.write(data + "\n")
    f.close();

def write_file(file, data):
    f=open(file,'a')
    f.write(data + "\n")
    f.close(); 
        
def create_file(username):
    directory = 'cache'
    file = directory + '/' + username + '.dat'
    f=open(file,'w')
    f.write(username)
    f.close();

def do_task(driver, username, password):
        
    
    # Login
    print ('[+] '+ timenow() +' User %s Logging in... ' % username)
    #driver.maximize_window()
    driver.set_window_size(1696, 600)
    driver.get(LOGIN_URL)
    ditulis = False
    
    WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        presence_of_all_elements_located((By.CSS_SELECTOR, SELECTOR_USERNAME))
    )
    username_elem = driver.find_element_by_css_selector(SELECTOR_USERNAME)
    username_elem.send_keys(username)
    
    password_elem = driver.find_element_by_css_selector(SELECTOR_PASSWORD)
    password_elem.send_keys(password)
    
    login_elem = driver.find_element_by_css_selector(SELECTOR_LOGIN)
    ActionChains(driver).move_to_element(login_elem).perform()
    login_elem.click()
    
    WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        presence_of_all_elements_located((By.CSS_SELECTOR, SELECTOR_LOGGED_IN))
    )
    
    # Go to first products page and get the amount of pages
    print ('[+] '+ timenow() +' Find total page... ')
    driver.get(FIRST_PAGE_URL)
    
    pages_links = WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        lambda driver: driver.find_elements_by_css_selector(SELECTOR_PAGINATOR_ELEMS)[1:-1]
    )
    pages_count = int(pages_links[-1].text.strip())
    print ('[+] '+ timenow() +' Total page is  %d, go to random page... ' % pages_count)
    
    clicks = 0
    current_page = 1
    
    while clicks < MAX_CLICKS:
        # Go to a random page
        
        products = WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
            lambda driver: driver.find_elements_by_css_selector(SELECTOR_PRODUCTS)
        )
        
        random_page = random.randint(1, pages_count)
        go_to_page_elem = driver.find_element_by_css_selector(SELECTOR_GO_TO_PAGE)
        driver.execute_script('arguments[0].value="%d";' %random_page, go_to_page_elem);
        go_to_page_elem.send_keys(Keys.RETURN)
        
        if random_page != current_page:
            current_page = random_page
            WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(staleness_of(products[0]))
            WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
                lambda driver: driver.find_elements_by_css_selector(SELECTOR_PRODUCTS)
            )
        
        # Click a random element
        
        elems_to_click = driver.find_elements_by_css_selector(SELECTOR_ELEMS_TO_CLICK)

        i = 0

        while i < len(elems_to_click):
            elem_to_click = elems_to_click[i]
            #if elem_to_click.text != ELEM_TO_CLICK_TEXT_ID and elem_to_click.text != ELEM_TO_CLICK_TEXT_EN:
            if elem_to_click.get_attribute('innerHTML') != ELEM_TO_CLICK_TEXT_ID and elem_to_click.text != ELEM_TO_CLICK_TEXT_EN:
                elems_to_click.remove(elem_to_click)
            else:
                i += 1
        
        if not elems_to_click:
            print ('[+] '+ timenow() +' There is no Boost Button enabled...Go to next user...')
            break
        
        random_elem_to_click = random.choice(elems_to_click)
        #random_elem_to_click = elems_to_click[1]
        list_item_elem = random_elem_to_click.find_element_by_xpath('../../../..')
        parent_random_elem = random_elem_to_click.find_element_by_xpath('..')

        ActionChains(driver).move_to_element(list_item_elem).perform()
        time.sleep(1)

        ActionChains(driver).move_to_element(parent_random_elem).perform()
        time.sleep(1)

        ActionChains(driver).move_to_element(random_elem_to_click).perform()
        time.sleep(1)

        try:
            random_elem_to_click.click()
        except (WebDriverException) as err:
            if 'Other element would receive the click' in str(err):  # the chat button
                driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')
                ActionChains(driver).move_to_element(random_elem_to_click).perform()
                time.sleep(1)
                random_elem_to_click.click()
            else:
                raise err
        
        WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(staleness_of(random_elem_to_click))
        create_file(username)
        print ('[+] '+ timenow() +' User %s clicked a product on page %d' %(username, current_page))
            
        if ditulis == False :
            sukses('[+] '+ datetimenow() +' User %s berhasil nyundul...' % username)
            ditulis = True
        
        clicks += 1
        
        if clicks == MAX_CLICKS:
            print ('[+] '+ timenow() +' Already click 5 times, go to next user...')


def banner():
    print ("=============================================")
    print ("[+]                        _       _  "       )
    print ("[+]    ___ _   _ _ __   __| |_   _| | "       )
    print ("[+]   / __| | | | '_ \ / _` | | | | | "       )
    print ("[+]   \__ \ |_| | | | | (_| | |_| | | "       )
    print ("[+]   |___/\__,_|_| |_|\__,_|\__,_|_| "       )
    print ("[+] -=Shopee Auto Sundul By : Ryanaby=- "     )
    print ("=============================================")

def main():
    banner()
    # Load credentials
    
    credentials = []
    if os.path.isfile(LIST_ACCOUNT) != True:
       exit()

    for line in open(LIST_ACCOUNT):
        if line.strip():
            credentials.append(
                [
                    line[:line.index(':')].strip(), 
                    line[line.index(':') + 1:].strip()
                ]
            )

    # Forever loop
    
    while credentials:
        start_time = time.time()
        for username, password in credentials:
            if can_exe(username) != True:
                continue
            
            _chrome_options = Options()
            _chrome_options.add_argument('disable-infobars')
            _chrome_options.add_argument('disable-notifications ')
            driver = webdriver.Chrome(chrome_options=_chrome_options)
            try:
                do_task(driver, username, password)
            except (Exception) as err:
                print 
                if err.__class__.__name__ == "TimeoutException" :
                    create_file(username)
                    write_file('timeout.txt', username + ":" + password)
                print ('[+] '+ timenow() +' An unhandled exception has ocurred with username "%s". ' \
                      'Original error was: %s: %s. Continuing execution...' \
                      %(username, err.__class__.__name__, err))
            driver.quit()
            
        #total_time = time.time() - start_time
        #sleep_time = TIME_TO_SLEEP - total_time
        
        print ('[+] '+ timenow() +' Sleeping %s...' % format_time(TIME_TO_SLEEP))
        print ('[+] '+ timenow() +' Will run again at %s...' % next_time(TIME_TO_SLEEP))
        time.sleep(TIME_TO_SLEEP)


if __name__ == '__main__':
    main()
