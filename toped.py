import os
import sys
import random
import time
import datetime
import pickle

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_all_elements_located, \
    staleness_of
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import WebDriverException

#LIST_ACCOUNT = sys.argv[1]
LIST_ACCOUNT = 'toped.txt'
CHROME_DRIVER = 'chromedriver.exe'
#CHROME_DRIVER = 'QloBOT\driver\chromedriver.exe'
LOGIN_URL = 'https://www.tokopedia.com/login'
FIRST_PAGE_URL = 'https://www.tokopedia.com/manage-product-new.pl?nref=pdlsthead&perpage=80'
PAGE_LOAD_TIMEOUT = 60
MAX_CLICKS = 5
directory = 'tpsession'

TIME_TO_SLEEP = 5 * 60

SELECTOR_USERNAME = '[name*="email"]'
SELECTOR_PASSWORD = '[name*="password"]'
#SELECTOR_LOGIN = 'button.shopee-button.shopee-button--primary.shopee-button--block'
SELECTOR_LOGIN = '.js__submit-login.btn.btn-action.user-accounts-form__button'
SELECTOR_LOGGED_IN = '.new-dropdown.usernav.user'
SELECTOR_PRODUCTS_ELEMS = '.box-media.mt-5'


def timenow():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')

def datetimenow():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    
def next_time(tambah):
    return datetime.datetime.fromtimestamp(time.time() + tambah).strftime('%H:%M:%S')

def format_time(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d jam %02d menit %02d detik" % (h, m, s)

def can_exe(username):
    exe = True
    if not os.path.exists(directory):
        os.makedirs(directory)

    file = directory + '/' + username + '.pkl'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if os.path.exists(file):
        if (time.time() - os.path.getmtime(file)) < 3600:
            lagi = os.path.getmtime(file) + 3600
            print ('[+] '+ timenow() +' User '+ username +' akan nyundul lagi pada... ' + datetime.datetime.fromtimestamp(lagi).strftime('%Y-%m-%d %H:%M:%S'))
            exe = False
    
    return exe
	
	
def cookie_check(driver, username, password):
    exe = True
    file = directory + '/' + username + '.pkl'
	
		
    if not os.path.exists(file):
        login(driver, username, password)
        return exe

    if os.path.exists(file):
        print ('[+] '+ timenow() +' User %s Logging in with cookies' % username)
        driver.get(LOGIN_URL)
        cookies = pickle.load(open(file, "rb"))
        for cookie in cookies:
            driver.add_cookie(cookie)
        do_task(driver, username, password)
    

        
def sukses(data):
    file = 'sukses.txt'
    f=open(file,'a')
    f.write(data + "\n")
    f.close();

def write_file(file, data):
    f=open(file,'a')
    f.write(data + "\n")
    f.close(); 
        
	
def write_cookie(driver, username):
    pickle.dump( driver.get_cookies() , open(directory + '/' + username + ".pkl","wb"))

def login(driver, username, password):
    print ('[+] '+ timenow() +' User %s Logging in... ' % username)
    #driver.maximize_window()
    driver.set_window_size(1696, 600)
    driver.get(LOGIN_URL)
    ditulis = False
    
    WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        presence_of_all_elements_located((By.CSS_SELECTOR, SELECTOR_USERNAME))
    )
    username_elem = driver.find_element_by_css_selector(SELECTOR_USERNAME)
    username_elem.send_keys(username)
    
    password_elem = driver.find_element_by_css_selector(SELECTOR_PASSWORD)
    password_elem.send_keys(password)
    
    login_elem = driver.find_element_by_css_selector(SELECTOR_LOGIN)
    ActionChains(driver).move_to_element(login_elem).perform()
    login_elem.click()
	
    try:
        WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
            presence_of_all_elements_located((By.CSS_SELECTOR, SELECTOR_LOGGED_IN))
        )
        print ('[+] '+ timenow() +' Logged In... ')
        write_cookie(driver, username)
        do_task(driver, username, password)
    except WebDriverException:
        write_file(OUTPUTFILE, username + " FAIL")
        print ('[+] '+ timenow() +' FAIL ')
	

def do_task(driver, username, password):
        
    
    driver.get(FIRST_PAGE_URL)
    
    elems_to_click = WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        lambda driver: driver.find_elements_by_css_selector(SELECTOR_PRODUCTS_ELEMS)
    )
    


    
    random_elem_to_click = random.choice(elems_to_click)
    write_cookie(driver, username)
    item_elem = random_elem_to_click.find_element_by_css_selector(".break-link.fs-13.fw-600.el")
    print ('[+] '+ timenow() +' Openning '+ item_elem.text)
    item_elem.click()

    sundulbtn = WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
        lambda driver: driver.find_element_by_css_selector('.rvm-button-promo')
    )
    sundulbtn.click()
    try:
        WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(
            presence_of_all_elements_located((By.XPATH, "//*[contains(text(), 'Promo pada produk')]"))
        )
        print ('[+] '+ timenow() +' Sundul Sukses ')
    except WebDriverException:
        print ('[+] '+ timenow() +' Sundul Gagal')

    

    



 
        
       

def banner():
    print ("=============================================")
    print ("[+]                        _       _        " )
    print ("[+]    ___ _   _ _ __   __| |_   _| |       " )
    print ("[+]   / __| | | | '_ \ / _` | | | | |       " )
    print ("[+]   \__ \ |_| | | | | (_| | |_| | |       " )
    print ("[+]   |___/\__,_|_| |_|\__,_|\__,_|_|       " )
    print ("[+] -=Toped Sundul based on Ryanaby Shopee=-" )
    print ("=============================================")

def main():
    banner()
    # Load credentials
    
    credentials = []
    if os.path.isfile(LIST_ACCOUNT) != True:
       exit()

    for line in open(LIST_ACCOUNT):
        if line.strip():
            credentials.append(
                [
                    line[:line.index(':')].strip(), 
                    line[line.index(':') + 1:].strip()
                ]
            )

    # Forever loop
    
    while credentials:
        start_time = time.time()
        for username, password in credentials:
            if can_exe(username) != True:
                continue
            
            prefs = {"profile.managed_default_content_settings.images": 2}
            _chrome_options = Options()
            _chrome_options.add_argument('disable-infobars')
            _chrome_options.add_argument('disable-notifications ')
            _chrome_options.add_experimental_option("prefs",prefs)
            driver = webdriver.Chrome(chrome_options=_chrome_options)
            try:
                cookie_check(driver, username, password)
            except (Exception) as err:
                print 
                if err.__class__.__name__ == "TimeoutException" :
                    write_file('timeout.txt', username + ":" + password)
                print ('[+] '+ timenow() +' An unhandled exception has ocurred with username "%s". ' \
                      'Original error was: %s: %s. Continuing execution...' \
                      %(username, err.__class__.__name__, err))
            driver.quit()
            
        #total_time = time.time() - start_time
        #sleep_time = TIME_TO_SLEEP - total_time
        
        print ('[+] '+ timenow() +' Sleeping %s...' % format_time(TIME_TO_SLEEP))
        print ('[+] '+ timenow() +' Will run again at %s...' % next_time(TIME_TO_SLEEP))
        time.sleep(TIME_TO_SLEEP)


if __name__ == '__main__':
    main()
