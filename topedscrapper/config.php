<?php

	function prepare($url){
		$result  = array();
		$get     = html_entity_decode(browsing($url));

		$html    = str_get_html($get);
		$scripts = get_product_script($html);
		if( !$html ) return $result;
		if( !$scripts ) return $result;
		
		if ( $result = get_type_page($scripts, $html, $get) ){
			if( $result['type'] == 'category'){
				if( $categories = get_category_toped($result['cat_id']) ){
					$file = strtoupper($result['type']);
					if( isset($categories['parent']['name']) ){
						$file .= " - {$categories['parent']['name']}";
					}
					
					if( isset($categories['sub']['name']) ){
						$file .= " - {$categories['sub']['name']}";
					}
					
					$file .= " - {$categories['name']}.csv";
					
					$result['file'] = $file;
				}
			}
			
			if( $result['type'] == 'seller'){
				$result['file'] = strtoupper($result['type']) . " - {$result['shop_name']}.csv";
			}
			
			if( $result['type'] == 'hot'){
				$result['file'] = strtoupper($result['type']) . " - {$result['hot_name']}.csv";
			}
		}

		return $result;

	}

    function build_link( $params = array(), $type ) {
		$api_v26 ='https://ace.tokopedia.com/search/v2.6/product?';
		$api_v3 = 'https://ace.tokopedia.com/search/product/v3?';
		 
		$param['full_domain'] = 'www.tokopedia.com';                                              //ga tau, diemin aja
		$param['scheme']      = 'https';                                                          //ga tau, diemin aja
		$param['device']      = 'mobile';                                                         //ga tau, diemin aja
		$param['ob']          = '8';                                                              //order By, di urutkan berdasarkan...
		$param['condition']   = '1';                                                              //Kondisi, 1=baru
		$param['rows']        = '40';                                                             //
		$param['start']       = '0';                                                              //
		
		if( $type == 'category'){
			$param['fcity']       = '146,150,151,167,168,171,175,176,177,178,144,463,174';        //Kota yg akan di scrape
			$param['shipping']    = '1,12,13';                                                    //Dukungan kurir
			//$param['rt']          = '5,4,3,2,1';                                                //Rating produk
			$param['source']      = 'directory';                                                  //ga tau, diemin aja
			$param['page']        = '1';                                                          //
			$param['pmin']        = '500';                                                        //Harga minimum
			$param['image_size']  = '200';                                                        //

		}
		
		if( $type == 'seller'){
			$param['source']      = 'shop_product';                                               //ga tau, diemin aja
		}
		
		if( $type == 'hot'){
			$param['source']      = 'hot_product';                                                //ga tau, diemin aja
			$param['rt']          = '4,5';                                                        //Rating produk
		}
		
		if( $params ){
			foreach($params as $k=>$v){
				if( !preg_match('/^(u(rl)?|f(ile)?|t(ype)?|list|limit)$/i', $k) ){
					$param[$k] = $v;
				}
			}
		}

		if( $type == 'seller'){
			foreach($param as $k=>$v){
				if( preg_match('/^(fcity|shipping|page|p(min|max)|image_size)$/i', $k) ){
					unset($param[$k]);
				}
			}
		}
		
        $renew_url = $api_v3 . rawurldecode(http_build_query($param));

		return $renew_url;
	}

	function link_to_param( $url, $params ){
		
		$get_param = array_filter(array_map('trim', explode("?", $url)));

		if(isset($get_param[1]) && $get_param[1]){
			if( $list_param = preg_split('/&/', $get_param[1]) ){
				if( $renew_param = param_to_key_value($list_param) ){

					foreach($renew_param as $k=>$v){
						$params[$k] = $v;
					}
				}
			}
		}

		return $params;
	}

	function param_to_key_value( $params = array() ){
		$result = array();
		if($params){
			foreach($params as $param){
				$convert = preg_split('/=/', $param);
				if( count($convert) == 2 ){
					$key = $convert[0];
					$val = $convert[1];
					$result[$key] = $val;
				}
			}
		}

		return $result;
	}

	function get_product_script($html){
		$result = array();
		if($html){
			if( $scripts = $html->find('script') ){
				foreach($scripts as $k=>$v){
					$script = trim($v->innertext);
					if( $script ){
						$result[] = $script;
					}
				}
			}
		}

		return $result;
	}
	
	function get_type_page($scripts, $html, $string){
		$result   = array();
		$type     = null;
		$catid    = null;
		$catname  = null;
		$shopid   = null;
		$shopname = null;
		$hotid    = null;
		$hotalias = null;
		$hotname  = null;

		/* get id and type */

		if ( $scripts ) {
			foreach($scripts as $sc) {
				
				//"tags":"{\"categories\":{\"56\":\"ibu-bayi\"}}","id"
				if( preg_match('/"tags":"\{\\\"categories\\\":\{\\\"(\d+)\\\":\\\"(.*?)\\\"\}\}","id"/', $sc, $match) ){

					if( isset($match[1]) && isset($match[2]) ){
						if( $getcat = get_category_toped($match[1]) ){
							$result['type']     = 'category';
							$result['cat_id']   = trim($match[1]);
							$result['cat_name'] = $getcat['name'];
							break;
						}
					}
				
				// ,"id":1484,"name":"Jaket Gunung","url"
				}elseif( preg_match('/,"id":(\d+),"name":"(.*?)","url"/', $sc, $match) ){

					if( isset($match[1]) && isset($match[2]) ){
						if( $getcat = get_category_toped($match[1]) ){
							if( $getcat['name'] == trim($match[2]) ){
								$result['type']     = 'category';
								$result['cat_id']   = trim($match[1]);
								$result['cat_name'] = $getcat['name'];
								break;
							}
						}
					}

				//}elseif( preg_match('/"shop":\{"id":(\d+),.+true,"domain":"(.*?)"\}/', $sc, $match) ){
				}elseif( preg_match('/"domain":"(.*?)","shopID":"(\d+)","name":"(.*?)","tagLine"/', $sc, $match) ){
					if( isset($match[1]) && isset($match[2]) ){
						$result['type']      = 'seller';
						$result['shop_id']   = trim($match[2]);
						$result['shop_name'] = trim($match[1]);
						break;
					}

				}elseif ( preg_match('/(["\'])data(\g1)[:\s]+\{(\g1)id(\g1)[:\s]+(\d+)[,\s]+(\g1)aliasKey(\g1)[:\s]+(\g1)(.*?)(\g1)[,\s]+(\g1)title(\g1)[:\s]+(\g1)(.*?)(\g1)[,\s]+(\g1)description(\g1)/i', $sc, $match) ) {
					if( isset($match[5]) && (null !== $match[5]) ){
						$hotid = trim($match[5]);
					}
					
					if( isset($match[9]) && (null !== $match[9]) ){
						$hotalias = trim($match[9]);
					}
					
					if( isset($match[14]) && (null !== $match[14]) ){
						$hotname = trim($match[14]);
					}
					
					if( isset($hotid) && isset($hotalias) && isset($hotname) ){
						$result['type'] = 'hot';
						$result['hot_id'] = $hotid;
						$result['hot_alias'] = $hotalias;
						$result['hot_name'] = $hotname;
						foreach($scripts as $s){
							if ( preg_match('/\{(["\'])source(\g1)[:\s]+(\g1)hot_product(\g1)[,\s]+(\g1)hot_id(\g1)[:\s]+(\g1)(\d+)(\g1)(.*?)\}/i', $s, $match) ) {
								if( isJson($match[0]) ){
									$json = json_decode($match[0], true);
									foreach($json as $k=>$v){
										$result[$k] = $v;
									}
								}
							}
						}

						//return $result;
					}
				}
			}
		}

		return $result;
	}

	function is_expire($file){
		$expire = 3600;
		$ftime = filemtime($file);

		if ($expire > 0 && time() > $ftime + $expire)
		{
			//echo "file expire, need to delete\n";
			return true;
		}
		
		return false;

	}
	
	// Tokopedia
	function get_categories_toped(){
		$file = 'toped_categories.json';
		if( !file_exists($file) || is_expire($file) ){
			//$url = 'https://hades.tokopedia.com/v0/categories';
			$url = 'https://hades.tokopedia.com/v1/categories?filter=type==tree&safe_search=false';
			$get = browsing($url);
			if( isJson($get) ){
				$json = json_decode($get, true);
				if( isset($json['data']['categories']) ){
					$fp = fopen($file, 'w');
					fwrite($fp, json_encode($json['data']['categories'], true));
					fclose($fp);
				}
			}
		}
		
		if( $cat = bacaFile($file) ){
			return json_decode($cat, true);
		}
		
		return false;

	}
	
	function get_recursive_categories_toped( $data ){

		$result = array();
		$result[] = array('id' => $data['id'], 'name' => $data['name'], 'parent' => $data['parent']) ;

		if ( isset($data['child']) ){
			
			foreach( $data['child'] as $sub ){
				
				$result[] = array('id' => $sub['id'], 'name' => $sub['name'], 'parent' => $sub['parent']) ;
				
				if ( isset($sub['child']) ){
					foreach( $sub['child'] as $sub_sub ){
						$result[] = array('id' => $sub_sub['id'], 'name' => $sub_sub['name'], 'parent' => $sub_sub['parent']) ;
					}
				}
			}
		}

		return $result;

	}
	
	function get_toped_mapping(){
		//$file = 'toped_mapping.json';
		$file = 'toped_categories_mapping.json';
		
		if( !file_exists($file) || is_expire($file) ){
			$categories = array();
			
			foreach( get_categories_toped() as $parent ){
				$cat    = get_recursive_categories_toped( $parent );
				$parent = null;
				$sub    = array();
				
				$list_sub = array();

				foreach($cat as $c){
					if($c['parent'] == 0){
						$parent = $c;
						$key = $c['id'];
						$categories['parent'][$key]['name'] = $c['name'];
						break;
					}
				}

				foreach($cat as $c){
					$key = $c['id'];
					if( $c['parent'] != 0 && $c['parent'] == $parent['id'] ){
						$categories['sub'][$key]['name'] = $c['name'];
						$categories['sub'][$key]['parent'] = $parent;
						$list_sub[] = $c;
					}
				}
				
				foreach($cat as $c){
					$key = $c['id'];
					if( $list_sub ){
						foreach($list_sub as $sub){
							if( $c['parent'] != 0 && $c['parent'] == $sub['id']){
								$categories['cat'][$key]['name'] = $c['name'];
								$categories['cat'][$key]['sub'] = $sub;
								$categories['cat'][$key]['parent'] = $parent;
							}
						}
					}

				}

			}

			if( $categories ){
				$fp = fopen($file, 'w');
				fwrite($fp, json_encode($categories, true));
				fclose($fp);
			}
		}
		
		if( $cats = bacaFile($file) ){
			return json_decode($cats, true);
		}
	}
	
	function get_category_toped($id){
		
		$categories = get_toped_mapping();
		
		foreach($categories['parent'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}
		
		foreach($categories['sub'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}
		
		foreach($categories['cat'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}

		return false;
	}

	// Shopee
	
	function get_categories_shopee(){
		$file = 'shopee_categories.json';
		if( !file_exists($file)  || is_expire($file) ){
			$url = 'https://shopee.co.id/api/v1/category_list/';
			$get = browsing($url);
			if( isJson($get) ){
				$fp = fopen($file, 'w');
				fwrite($fp, $get);
				fclose($fp);
			}
		}
		
		if( $cat = bacaFile($file) ){
			return json_decode($cat, true);
		}
		
		return false;

	}

	function get_recursive_categories_shopee( $data ){
		
		$result = array();
		
		$result[] = array('id' => $data['main']['catid'], 'name' => $data['main']['display_name'], 'parent' => $data['main']['parent_category']) ;
		
		if ( isset($data['sub']) ){
			
			foreach( $data['sub'] as $sub ){
				
				$result[] = array('id' => $sub['catid'], 'name' => $sub['display_name'], 'parent' => $sub['parent_category']) ;
				
				if ( isset($sub['sub_sub']) ){
					foreach( $sub['sub_sub'] as $sub_sub ){
						$result[] = array('id' => $sub_sub['catid'], 'name' => $sub_sub['display_name'], 'parent' => $sub['catid']) ;
					}
				}
			}
		}

		return $result;

	}

	function get_shopee_mapping(){
		$file = 'shopee_categories_mapping.json';
		
		if( !file_exists($file) ){
			$categories = array();
			
			foreach( get_categories_shopee() as $parent ){
				$cat    = get_recursive_categories_shopee( $parent );
				$parent = null;
				$sub    = array();
				
				$list_sub = array();

				foreach($cat as $c){
					if($c['parent'] == 0){
						$parent = $c;
						$key = $c['id'];
						$categories['parent'][$key]['name'] = $c['name'];
						break;
					}
				}

				foreach($cat as $c){
					$key = $c['id'];
					if($c['parent'] == $parent['id']){
						$categories['sub'][$key]['name'] = $c['name'];
						$categories['sub'][$key]['parent'] = $parent;
						$list_sub[] = $c;
					}
				}
				
				foreach($cat as $c){
					$key = $c['id'];
					foreach($list_sub as $sub){
						if($c['parent'] == $sub['id']){
							$categories['cat'][$key]['name'] = $c['name'];
							$categories['cat'][$key]['sub'] = $sub;
							$categories['cat'][$key]['parent'] = $parent;
						}
					}
				}

			}

			if( $categories ){
				$fp = fopen($file, 'w');
				fwrite($fp, json_encode($categories, true));
				fclose($fp);
			}
		}
		
		if( $cats = bacaFile($file) ){
			return json_decode($cats, true);
		}
	}

	function get_category_shopee($id){
		
		$categories = get_shopee_mapping();
		
		foreach($categories['parent'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}
		
		foreach($categories['sub'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}
		
		foreach($categories['cat'] as $key=>$val){
			if( $key == $id  ){
				return $val;
			}
		}

		return false;
	}


