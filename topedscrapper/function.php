<?php

	function browsing($url, $method = 'get', $postdata = null, $refer = null, $header = null, $timeout = 20,$useragent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0')
	{
		$useragent= 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';
		$s = curl_init();
 
		curl_setopt($s,CURLOPT_URL, $url);
		if ($header) 
			curl_setopt($s,CURLOPT_HTTPHEADER, $header);
		if ($refer)
			curl_setopt($s, CURLOPT_REFERER, $refer);
		curl_setopt($s,CURLOPT_TIMEOUT, $timeout);
		curl_setopt($s,CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($s,CURLOPT_MAXREDIRS, 3);
		curl_setopt($s,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s,CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($s,CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($s,CURLOPT_COOKIEFILE, 'cookie.txt'); 
		curl_setopt($s,CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($s,CURLOPT_SSL_VERIFYHOST,  2);
		if(strtolower($method) == 'post')
		{
			curl_setopt($s,CURLOPT_POST, true);
			curl_setopt($s,CURLOPT_POSTFIELDS, $postdata);
		}
		else if(strtolower($method) == 'delete')
		{
			curl_setopt($s,CURLOPT_CUSTOMREQUEST, 'DELETE');
		}
		else if(strtolower($method) == 'put')
		{
			curl_setopt($s,CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($s,CURLOPT_POSTFIELDS, $postdata);
		}
		curl_setopt($s,CURLOPT_HEADER, 0);			 
		curl_setopt($s,CURLOPT_USERAGENT, $useragent);
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);
		$html = curl_exec($s);
		$status = curl_getinfo($s, CURLINFO_HTTP_CODE);
		curl_close($s);
		return $html;
	}

	function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
	function escaping_regex($string,$excludes = null) {
		$str_escape  = array('#','*','+','?','!','/','^','.','$','|','(',')','[',']','{','}');
		$arr_escape = array();
		$arr_replace = array();
		
		$ex = array_map('trim',explode(',',$excludes));
		foreach($str_escape as $esc){
			if (!in_array($esc, $ex)) {
				$arr_escape[]  = $esc;
				$arr_replace[] = '\\' . $esc;
			}
		}
		$newstr = str_replace($arr_escape,$arr_replace,$string);
		return $newstr;
	}
	
	function bacaFile($file){
		if(!file_exists($file)){
			echo "[!] File {$file} Not Exists\n";
			return false;
		}
		if (function_exists('file_get_contents')){
			return file_get_contents($file);
		}elseif(function_exists('fopen') && function_exists('fread')){
			$fp = fopen($file, "r");
			if ( !$fp ) {
			   throw new Exception("[!] Failed open file {$file}.");
			   return false;
			}
			$content = fread($fp, filesize($file));
			fclose($fp);
			return $content;
		}
		echo "[!] Failed read {$file}\n";
		return false;
	}
	
	function validate_path($path, $dir = false){
		$info = pathinfo($path);
		$dirname = $info['dirname'];		
		if(file_exists($dirname) && !is_dir($dirname)){
			if(!unlink($dirname)){
				echo "[!] Unable to delete file {$dirname}\n";
				return false;
			}
		}
		if(!file_exists($dirname)){
			if(!mkdir($dirname,0755,true)){
				echo "[!] Unable to create dir {$dirname}\n";
				return false;
			}
		}
		if(file_exists($dirname) && is_dir($dirname)){
			if($dir){
				return realpath($dirname);
			}
			return $path;
		}
		
		return false;
	}
	
	function str_contains( $str, $contain ){
		if( strpos( $str, $contain ) !== false)
			return true;
		return false;
	}
	
	function GetBetween($content,$start,$end){
		$r = explode($start, $content);
		if (isset($r[1])){
			$r = explode($end, $r[1]);
			return $r[0];
		}
		return '';
	}
	
	function convert_berat($berat) {
		$berat = trim(str_replace('.', '', $berat));
		if(str_contains($berat, 'gr')) {
			return str_replace('gr','', $berat);
		} else if(str_contains($berat, 'kg')) {
			$berat = str_replace('kg','', $berat);
			return 1000 * $berat;
		} else {
			exit("Berat belum diketahui coy...\n");
		}
	}
	
	function url_to_filename($url) {
		$file = 'file.csv';
		$slug = preg_replace('/https?:\/\/(www\.)?tokopedia\.com\/(p\/)?/i', '', $url);
		if( $xx = preg_split('/[\/]+/', $slug) ){
			$rx[] = $xx[0];
			if(isset($xx[1])){
				if( $xxx = preg_split('/[\?\&]+/', $xx[1]) ){
					 $rx[] = $xxx[0];
				}
			}
			$file = implode('-', $rx) . '.csv';
		}
		
		
		return $file;
	}
	
	function generateCsv(array $data, $filename,  $delimiter = ',', $enclosure = '"') {
		$handle = fopen($filename, 'r+');
		foreach ($data as $line) {
			fputcsv($handle, $line, $delimiter, $enclosure);
		}
		rewind($handle);
		$contents = '';
		while (!feof($handle)) {
			$contents .= fread($handle, 8192);
		}
		fclose($handle);
		return $contents;
	}
	
	function bersih_url($url) {
		$url = trim($url);
		if(str_contains( $url, '?trkid=')) {
			$x = explode('?trkid=', $url);
			return $x[0];
		}
		
		return $url;
	}
	
	function is_sudah($url){
		$wp    = 'cache/wpcache-'.PROJECT;
		$name  = md5($url);
		if(!file_exists($wp)){
			mkdir($wp,0775,true);
		}elseif(file_exists($wp) && !is_dir($wp)){
			unlink($wp);
			mkdir($wp,0775,true);
		}
				
		$subdir = $wp .'/'. substr($name,0,3);
		if(!file_exists($subdir)){
			mkdir($subdir,0775,true);
		}elseif(file_exists($subdir) && !is_dir($subdir)){
			unlink($subdir);
			mkdir($subdir,0775,true);
		}
		$cache = $subdir .'/'. $name . '.cache';
		if(!file_exists($cache)){
			file_put_contents($cache,$url);
			return false;
		}else{
			return true;
		}
	}	


