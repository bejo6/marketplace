<?php
    function myheader($filename, $options = array()){
        echo "=============================================\n";
        echo "[*] Tokopedia Scrapper\n";
        echo "[*] Author     : bejo6\n";
        echo "[*] Credits    : Tokopedia Scrapper by Ryan Aby\n";
        echo "[*] Version    : 1.0.4\n\n";
		if( !$options ){
			echo "[*] Usage      : (i)  php {$filename} -u <url> [options...]\n";
			echo "                 (ii) php {$filename} --list list_file.txt [options...]\n\n";
			echo "[*] Example\n";
			echo "    (i)  php {$filename} -u https://www.tokopedia.com/fashionstore\n";
			echo "         php {$filename} --list list_file.txt\n\n";
			echo "    (ii) With options\n";
			echo "         php {$filename} -u https://www.tokopedia.com/p/kategori-fashion-pria -s 8\n";
			echo "         php {$filename} -u https://www.tokopedia.com/p/kategori-fashion-pria --ob penjualan\n";
			echo "         php {$filename} -u https://www.tokopedia.com/p/kategori-fashion-pria --order-by=\"penjualan\"\n";
			echo "         php {$filename} --list list_file.txt -s 8\n";
			echo "         php {$filename} --list list_file.txt --ob penjualan\n";
			echo "         php {$filename} --list list_file.txt --order-by=\"penjualan\"\n\n";
			echo "    (iii) Auto get options\n";
			echo "         php {$filename} -u \"https://www.tokopedia.com/p/kategori-fashion-pria?ob=8&pmin=100000&pmax=10000000\"\n";
			echo "         php {$filename} --url=\"https://www.tokopedia.com/p/kategori-fashion-pria?ob=8&pmin=100000&pmax=10000000\"\n\n";
			echo "[*] try 'php {$filename} --help' for more information\n";
		}

        echo "=============================================\n\n";
    }
	
	function get_options($file){
		$result = array();
		
		$shortopts  = "";
		$shortopts .= "u:";    // url                                    [string]
		$shortopts .= "p:";    // page                                   [string]
		$shortopts .= "f:";    // file                                   [string]
		$shortopts .= "s:";    // sort / order by         (ob)           [string/init]
		$shortopts .= "l:";    // Lokasi                  (fcity)        [string/init] multi value delimiter by commas
		$shortopts .= "v:";    // Warna                   (variants)     [int] multi value delimiter by commas
		$shortopts .= "x:";    // harga minimum           (pmin)         [int]
		$shortopts .= "y:";    // harga maximum           (pmax)         [int]
		$shortopts .= "b:";    // Merek                   (brand)        [int] multi value delimiter by commas
		$shortopts .= "e:";    // Ekspedisi / Pengiriman  (shipping)     [int] multi value delimiter by commas
		$shortopts .= "c:";    // Kondisi                 (condition)    [int] multi value delimiter by commas
		$shortopts .= "r:";    // Rating                  (rt)           [int] multi value delimiter by commas
		
		$longopts  = array(
			"help::",          // help
							   
			"url::",           // url                                    [string]
			"page::",          // page                                   [string]
			"file::",          // file                                   [string]

			"list:",           // List Link                              [string]
			"limit:",          // Jumlah Limit per link                  [string]
			"ob:",             // order by                (ob)           [string/init]
			"order-by::",      // order by                (ob)           [string/init]
																		 
			"fcity:",          // Lokasi                  (fcity)        [string/init] multi value delimiter by commas
			"city::",          // Lokasi                  (fcity)        [string/init] multi value delimiter by commas
			"kota::",          // Lokasi                  (fcity)        [string/init] multi value delimiter by commas
																		 
			"variants:",       // Warna                   (variants)     [int] multi value delimiter by commas
			"color::",         // Warna                   (variants)     [int] multi value delimiter by commas
			"warna::",         // Warna                   (variants)     [int] multi value delimiter by commas
																		 
			"pmin:",           // harga minimum           (pmin)         [int]
			"min::",           // harga minimum           (pmin)         [int]
																		 
			"pmax:",           // harga maximum           (pmax)         [int]
			"max::",           // harga maximum           (pmax)         [int]
																		 
			"brand:",          // Merek                   (brand)        [int] multi value delimiter by commas
			"merek::",         // Merek                   (brand)        [int] multi value delimiter by commas
																		 
			"shipping:",       // Pengiriman              (shipping)     [int] multi value delimiter by commas
			"ship::",          // Pengiriman              (shipping)     [int] multi value delimiter by commas
																		 
			"condition:",      // Kondisi                 (condition)    [int] multi value delimiter by commas
			"kondisi::",       // Kondisi                 (condition)    [int] multi value delimiter by commas
																		 
			"rt:",             // Rating                  (rt)           [int] multi value delimiter by commas
			"rating::",        // Rating                  (rt)           [int] multi value delimiter by commas
														 
			"wholesale",       // Grosir                  (wholesale)    [boolean]
			"grosir",          // Grosir                  (wholesale)    [boolean]
														  
			"cashback",        // Cashback                (cashback)     [boolean]
														  
			"goldmerchant",    // Gold Merchant           (goldmerchant) [boolean]
														  
			"official",        // Official Store          (official)     [boolean]
														 
			"kreasi_lokal",    // Kreasi Lokal            (kreasi_lokal) [boolean]
														  
			"freereturns",     // Free Returns            (freereturns)  [boolean]
														  
			"preorder",        // Preorder                (preorder)     [boolean]

		);
		
		$options = getopt($shortopts, $longopts);		
		
		if( isset($options['help']) ){
			$help = trim($options['help']);
			if( preg_match('/^(u|url)$/i', $help, $match) ){
				print_option_url($match[1]);
			}elseif( preg_match('/^(list)$/i', $help, $match) ){
				print_option_list_url($match[1]);
			}elseif( preg_match('/^(limit)$/i', $help, $match) ){
				print_option_limit($match[1]);
			}elseif( preg_match('/^(p|page)$/i', $help, $match) ){
				print_option_page($match[1]);
			}elseif( preg_match('/^(f|file)$/i', $help, $match) ){
				print_option_file($match[1]);
			}elseif( preg_match('/^(s|ob|order-by)$/i', $help, $match) ){
				print_order_by($match[1]);
			}elseif( preg_match('/^(l|f?city|kota)$/i', $help, $match) ){
				print_list_city($match[1]);
			}elseif( preg_match('/^(v|variants|color|warna)$/i', $help, $match) ){
				print_option_color($match[1]);
			}elseif( preg_match('/^((x|y)|p?(min|max))$/i', $help, $match) ){
				print_option_rangeprice($match[1]);
			}elseif( preg_match('/^(b|brand|merek)$/i', $help, $match) ){
				print_option_brand($match[1]);
			}elseif( preg_match('/^(e|ship(ping)?)$/i', $help, $match) ){
				print_option_shipping($match[1]);
			}elseif( preg_match('/^(c|condition|kondisi)$/i', $help, $match) ){
				print_list_condition($match[1]);
			}elseif( preg_match('/^(r|rt|rating)$/i', $help, $match) ){
				print_option_rating($match[1]);
			}elseif( preg_match('/^(wholesale|grosir|cashback|goldmerchant|official|kreasi_lokal|freereturns|preorder)$/i', $help, $match) ){
				print_option_boolean($match[1]);
			}else{
				print_help($file);
			}
		}

		if( !isset($options['u']) && !isset($options['url']) && !isset($options['list']) ) {
			myheader($file);
			exit;
		}
		
		if( (isset($options['u']) && !$options['u']) || (isset($options['url']) && !$options['url'])  || (isset($options['list']) && !$options['list']) ){
			myheader($file);
			exit;
		}


		if($options){
			foreach($options as $k=>$v){
				if( preg_match('/^u|url$/i', $k) ){
					$result['url'] = $v;
					//if(preg_match('/[\?&]+/', $v)){
					//	return $result;
					//}
				}elseif( preg_match('/^(list)$/i', $k) ){
					$result['list'] = $v;
					//if(preg_match('/[\?&]+/', $v)){
					//	return $result;
					//}
				}elseif( preg_match('/^(p|page)$/i', $k) ){
					if( $val = preg_replace('/\D+/', '', $v)){
						$result['page'] = $val;
					}
				}elseif( preg_match('/^(limit)$/i', $k) ){
					if( $val = preg_replace('/\D+/', '', $v)){
						$result['limit'] = $val;
					}
				}elseif( preg_match('/^(f|file)$/i', $k) ){
					$result['file'] = trim($v);
				}elseif( preg_match('/^(s|ob|order-by)$/i', $k) ){
					foreach(list_order_by() as $kk=>$vv){
						if(preg_match("/{$kk}/i", trim($v))){
							$result['ob'] = $kk;
							break;
						}
						if(preg_match("/{$vv}/i", trim($v))){
							$result['ob'] = $kk;
							break;
						}
					}
				}elseif( preg_match('/^(l|f?city|kota)$/i', $k) ){
					$list_city = array();
					$citys     = array_filter(array_map('trim', explode(",", $v)));

					foreach($citys as $city){
						foreach(list_city() as $kk=>$vv){
							if( $city == $vv ){
								$list_city[] = $vv;
								break;
							}
							if( preg_match("/{$city}/i", $kk) ){
								$list_city[] = $vv;
								break;
							}
						}
					}
					
					if($list_city){
						$result['fcity'] = implode(",", $list_city);
					}
					

				}elseif( preg_match('/^(v|variants|color|warna)$/i', $k, $match) ){
					$v_variants = preg_replace('/[^\d\s\,]+/', '' , $v);
					$v_variants = implode( ",", array_filter(array_map('trim', explode(",", $v_variants))) );
					if($v_variants){
						$result['variants'] = $v_variants;
					}
				}elseif( preg_match('/^x|p?min$/i', $k, $match) ){
					if($pmin = preg_replace('/\D+/', '', $v)){
						$result['pmin'] = $pmin;
					}
				}elseif( preg_match('/^y|p?max$/i', $k, $match) ){
					if($pmax = preg_replace('/\D+/', '', $v)){
						$result['pmax'] = $pmax;
					}
				}elseif( preg_match('/^(b|brand|merek)$/i', $k, $match) ){
					$v_brand = preg_replace('/[^\d\s\,]+/', '' , $v);
					$v_brand = implode( ",", array_filter(array_map('trim', explode(",", $v_brand))) );
					if($v_brand){
						$result['brand'] = $v_brand;
					}
				}elseif( preg_match('/^(e|ship(ping)?)$/i', $k, $match) ){
					$list_shipping = array();
					$shippings     = array_filter(array_map('trim', explode(",", $v)));

					foreach($shippings as $shipping){
						foreach(list_shipping() as $kk=>$vv){
							if( $shipping == $vv ){
								$list_shipping[] = $vv;
								break;
							}
							if( preg_match("/" . escaping_regex($shipping) . "/i", $kk) ){
								$list_shipping[] = $vv;
								break;
							}
						}
					}
					
					if($list_shipping){
						$result['shipping'] = implode(",", $list_shipping);
					}
				}elseif( preg_match('/^(c|condition|kondisi)$/i', $k, $match) ){
					$result['condition'] = $v;
					$list_condition = array();
					$conditions     = array_filter(array_map('trim', explode(",", $v)));

					foreach($conditions as $condition){
						foreach(list_condition() as $kk=>$vv){
							if( $condition == $kk ){
								$list_condition[] = $kk;
								break;
							}
							if( preg_match("/" . escaping_regex($condition) . "/i", $vv) ){
								$list_condition[] = $kk;
								break;
							}
						}
					}
					
					if($list_condition){
						$result['condition'] = implode(",", $list_condition);
					}
					
				}elseif( preg_match('/^(r|rt|rating)$/i', $k, $match) ){
					if( $val = preg_replace('/[^\d,]+/', '', $v)){
						$result['rt'] = $val;
					}
				}elseif( preg_match('/^(wholesale|grosir)$/i', $k, $match) ){
					$result['wholesale'] = 'true';
				}elseif( preg_match('/^(cashback|goldmerchant|official|kreasi_lokal|freereturns|preorder)$/i', $k, $match) ){
					$key_bool = strtolower($match[1]);
					$result[$key_bool] = 'true';
				}
			}
		}

		return $result;
	}

	function print_help($file){
		$msg  =  "Usage: php {$file} [options...]\n";
		$msg .=  "Required:\n";
		$msg .=  "(i)   -u, --url=<URL>          Category URL\n\n";
		$msg .=  "(ii)  --list <FILENAME>        List link\n\n";
		
		$msg .=  "Optional:\n";             
		$msg .=  "  -p, --page=<PAGE>        Page Number                   Default = 1\n\n";
		$msg .=  "  -f, --file=<FILENAME>    Save as custom file name      Default = auto generate\n\n";
		$msg .=  "  -s, --ob,                Sort Product/Order by         Default = 8 (Penjualan)\n";
		$msg .=  "      --order-by=<VALUE>\n\n";
		$msg .=  "  -l, --fcity,             Location Seller               Default = 144,146,150,151,167,168,171,174,175,176,177,178,463 (Jabodetabek)\n";
		$msg .=  "      --city=<VALUE>                                     multi value delimiter by commas\n";
		$msg .=  "      --kota=<VALUE>\n\n";
		$msg .=  "  -v, --variants,          Code Number Color Variation   Default = None\n";
		$msg .=  "      --color=<VALUE>                                    multi value delimiter by commas\n";
		$msg .=  "      --warna=<VALUE>\n\n";
		$msg .=  "  -x, --pmin,              Minimum Price                 Default = None\n";
		$msg .=  "      --min=<VALUE>\n\n";
		$msg .=  "  -y, --pmax,              Maximum Price                 Default = None\n";
		$msg .=  "      --max=<VALUE>\n\n";
		$msg .=  "  -b, --brand,             Brand                         Default = None\n";
		$msg .=  "      --merek=<VALUE>\n\n";
		$msg .=  "  -e, --shipping,          Expedition/Shipping           Default = 1 (JNE)\n";
		$msg .=  "      --ship=<VALUE>\n\n";
		$msg .=  "  -c, --condition,         Condition Product             Default = 1 (Baru)\n";
		$msg .=  "      --kondisi=<VALUE>\n\n";
		$msg .=  "  -r, --rt                 Rating Product                Default = None\n";
		$msg .=  "      --rating=<VALUE>                                   multi value delimiter by commas\n\n";
		$msg .=  "  --limit                  Set Limit Product per link    Default = All\n\n";
		$msg .=  "  --wholesale, --grosir    Set Wholesale                 Default = False\n\n";
		$msg .=  "  --cashback               Set Cashback                  Default = False\n\n";
		$msg .=  "  --goldmerchant           Set Gold Merchant             Default = False\n\n";
		$msg .=  "  --official               Set Official Store            Default = False\n\n";
		$msg .=  "  --kreasi_lokal           Set Product Local             Default = False\n\n";
		$msg .=  "  --freereturns            Set Free Returns              Default = False\n\n";
		$msg .=  "  --preorder               Set Preorder                  Default = False\n\n";
		$msg .=  "Use --help=<option name> for details.\n";
		$msg .=  "Ex: --help=u OR --help=url\n";
		echo $msg;
		exit;
	}

	function print_option_url($p){
		echo "Use: " . (strlen($p) > 1 ? "--{$p}=" : "-{$p} ") . "<URL>\n";
		echo "Example:\n";
		echo " (i)  -u https://www.tokopedia.com/p/kategori-fashion-pria\n";
		echo " (ii) --url=\"https://www.tokopedia.com/p/kategori-fashion-pria?ob=8&pmin=100000&pmax=10000000\"\n";
		exit;
	}

	function print_option_list_url($p){
		echo "Use: --{$p} <FILENAME>\n";
		echo "Example:\n";
		echo " (i)  --list list_link.txt\n";
		exit;
	}

	function print_option_limit($p){
		echo "Use: --{$p} <COUNT>\n";
		echo "Example:\n";
		echo " (i)  --{$p} 1000\n";
		exit;
	}
	
	function print_option_page($p){
		echo "Use: " . (strlen($p) > 1 ? "--{$p}=" : "-{$p} ") . "<PAGE>\n";
		echo "Example:\n";
		echo " (i)  -p 20\n";
		echo " (ii) --page=20\n";
		exit;
	}
	
	function print_option_file($p){
		echo "Use: " . (strlen($p) > 1 ? "--{$p}=" : "-{$p} ") . "<FILENAME>\n";
		echo "Example:\n";
		echo " (i)  -f kategori-fashion-pria\n";
		echo " (ii) --file=\"kategori fashion pria\"\n";
		exit;
	}
	
	function print_list_type($p){
		echo "Use: " . (strlen($p) > 1 ? "--{$p}=" : "-{$p} ") . "<TYPE>\n";
		echo "List type:\n";
		echo " - category\n";
		echo " - seller\n\n";
		echo "Example:\n";
		echo " (i)  -t seller\n";
		echo " (ii) --type=\"seller\"\n";
		exit;
	}

	function print_order_by($p){
		$len = strlen($p);
		echo "Use: ";
		if($len == 1) echo "-{$p} ";
		if($len == 2) echo "--{$p} ";
		if($len > 2) echo "--{$p}=";
		echo "<ORDER BY>\n";
		
		echo "List Order by:\n";
		foreach(list_order_by() as $k=>$v){
			echo " - {$k} OR {$v}\n";
		}
		
		echo "\n";
		echo "Example:\n";
		echo " (i)   -s 8 OR -s penjualan\n";
		echo " (ii)  --ob 8 OR -ob penjualan\n";
		echo " (iii) --order-by=8 OR --order-by=penjualan\n";
		
		exit;
	}

	function print_list_city($p){
		echo "Use: ";
		if($p == 'l') echo "-{$p} ";
		if($p == 'fcity') echo "--{$p} ";
		if(preg_match('/^(city|kota)$/', $p)) echo "--{$p}=";
		echo "<LOCATION SELLER>\n";
		echo "multi value delimiter by commas\n";

		echo "List City:\n";
		foreach(list_city() as $k=>$v){
			$len = strlen($v);
			if($len == 1) echo " - {$v}   OR '{$k}'\n";
			if($len == 2) echo " - {$v}  OR '{$k}'\n";
			if($len >= 3) echo " - {$v} OR '{$k}'\n";
			
		}
		
		echo "\n";
		echo "Example:\n";
		echo " (i)   -l 165 (for 'Kota Bandung')\n";
		echo "       -l 144,146,150,151,167,168,171,174,175,176,177,178,463 (for 'Jabodetabek')\n";
		echo "       -l 165,378 (for 'Kota Bandung' and 'Kota Makassar')\n";
		echo " (ii)  --fcity 165 (for 'Kota Bandung')\n";
		echo "       --fcity 144,146,150,151,167,168,171,174,175,176,177,178,463 (for 'Jabodetabek')\n";
		echo "       --fcity 165,378 (for 'Kota Bandung' and 'Kota Makassar')\n";
		echo " (iii) --city=165 (for 'Kota Bandung')\n";
		echo "       --city=144,146,150,151,167,168,171,174,175,176,177,178,463 (for 'Jabodetabek')\n";
		echo "       --city=165,378 (for 'Kota Bandung' and 'Kota Makassar')\n";
		echo " (iv)  --kota=165 (for 'Kota Bandung')\n";
		echo "       --kota=144,146,150,151,167,168,171,174,175,176,177,178,463 (for 'Jabodetabek')\n";
		echo "       --kota=165,378 (for 'Kota Bandung' and 'Kota Makassar')\n";
		
		exit;
	}

	function print_option_color($p){
		echo "Use: ";
		if($p == 'v') echo "-{$p} ";
		if($p == 'variants') echo "--{$p} ";
		if(preg_match('/^(color|warna)$/', $p)) echo "--{$p}=";
		echo "<CODE COLOR>\n";
		echo "multi value delimiter by commas\n";
		
		echo "\n";
		echo "Example:\n";
		echo " (i)   -v 1\n";
		echo "       -v 1,2\n";
		echo " (ii)  --variants 1\n";
		echo "       --variants 1,2\n";
		echo " (iii) --color=1\n";
		echo "       --color=1,2\n";
		echo " (iv)  --warna=1\n";
		echo "       --warna=1,2\n";
		
		exit;
	}
	
	function print_option_rangeprice($p){
		$opt = '';
		$min = false;
		$max = false;
		if(preg_match('/^x|y$/', $p)) $opt = "-{$p} ";
		if(preg_match('/^p(min|max)$/', $p)) $opt = "--{$p} ";
		if(preg_match('/^min|max$/', $p)) $opt = "--{$p}=";
		echo "Use: {$opt}<VALUE>\n";
		
		if(preg_match('/^x|p?min$/', $p)) $min = true;
		if(preg_match('/^y|p?max$/', $p)) $max = true;

		echo "Example:\n";

		if($min){
			echo " (i)   -x 15000\n";
			echo " (ii)  --pmin 15000\n";
			echo " (iii) --min=15000\n";
		}

		if($max){
			echo " (i)   -y 50000\n";
			echo " (ii)  --pmax 50000\n";
			echo " (iii) --max=50000\n";
		}

		exit;
	}
	
	function print_option_brand($p){
		$opt = '';
		if($p == 'b') $opt = "-{$p} ";
		if($p == 'brand') $opt = "--{$p} ";
		if($p == 'merek') $opt = "--{$p}=";
		echo "Use: {$opt}<CODE BRAND>\n";
		echo "multi value delimiter by commas\n\n";

		echo "Example:\n";
		echo " (i)   -b 661\n";
		echo "       -b 3,661\n";
		echo " (ii)  --brand 661\n";
		echo "       --brand 3,661\n";
		echo " (iii) --merek=661\n";
		echo "       --merek=3,661\n";

		exit;
	}
	
	function print_option_shipping($p){
		$opt = '';
		if($p == 'e') $opt = "-{$p} ";
		if($p == 'shipping') $opt = "--{$p} ";
		if($p == 'ship') $opt = "--{$p}=";
		echo "Use: {$opt}<CODE SHIPPING>\n";
		echo "multi value delimiter by commas\n\n";

		echo "List Shipping Method:\n";
		foreach(list_shipping() as $k=>$v){
			echo " - {$v} (for {$k} )\n";
		}
		echo "\n";

		echo "Example:\n";
		echo " (i)   -e 1\n";
		echo "       -e 1,2\n";
		echo " (ii)  --shipping 1\n";
		echo "       --shipping 1,2\n";
		echo " (iii) --ship=1\n";
		echo "       --ship=1,2\n";

		exit;
	}
	
	function print_list_condition($p){
		$opt = '';
		if($p == 'c') $opt = "-{$p} ";
		if($p == 'condition') $opt = "--{$p} ";
		if($p == 'kondisi') $opt = "--{$p}=";
		echo "Use: {$opt}<CONDITION>\n";

		echo "List Shipping Method:\n";
		foreach(list_condition() as $k=>$v){
			echo " - {$k} OR {$v}\n";
		}
		echo "\n";

		echo "Example:\n";
		echo " (i)   -c 2\n";
		echo "       -c Bekas\n";
		echo " (ii)  --condition 2\n";
		echo "       --condition Bekas\n";
		echo " (iii) --kondisi=2\n";
		echo "       --kondisi=Bekas\n";

		exit;
	}
	
	function print_option_rating($p){
		$opt = '';
		if($p == 'r') $opt = "-{$p} ";
		if($p == 'rt') $opt = "--{$p} ";
		if($p == 'rating') $opt = "--{$p}=";
		echo "Use: {$opt}<VALUE>\n";

		echo "List Rating: " . implode(", ", range(0,5)) . "\n\n";

		echo "Example:\n";
		echo " (i)   -r 5\n";
		echo "       -r 5,4\n";
		echo " (ii)  --rt 5\n";
		echo "       --rt 5,4\n";
		echo " (iii) --rating=5\n";
		echo "       --rating=5,4\n";

		exit;
	}
	
	function print_option_boolean($p){

		echo "Use: --{$p}    (set true for {$p})\n";
		echo "No value needed\n";

		exit;
	}
	
	function list_order_by(){
		$order_by[3] = 'Termurah';
		$order_by[4] = 'Termahal';
		$order_by[5] = 'Ulasan';
		$order_by[8] = 'Penjualan';
		$order_by[9] = 'Terbaru';
		
		return $order_by;
	}
	
	function list_shipping(){
		$shipping['Instant Courier'] = '10,12,13';
		$shipping['JNE']             = '1';
		$shipping['TIKI']            = '2';
		$shipping['Pos Indonesia']   = '4';
		$shipping['Wahana']          = '6';
		$shipping['First']           = '9';
		$shipping['Go-Send']         = '10';
		$shipping['SiCepat']         = '11';
		$shipping['Ninja Express']   = '12';
		$shipping['Grab']            = '13';
		$shipping['J&T']             = '14';
		$shipping['REX']             = '16';
		
		return $shipping;
	}
	
	function list_condition(){
		$condition[1] = 'Baru';
		$condition[2] = 'Bekas';
		
		return $condition;
	}

	function list_city(){
		$city['Jabodetabek'] = '144,146,150,151,167,168,171,174,175,176,177,178,463';
		$city['DKI Jakarta'] = '174,175,176,177,178';
		$city['Kepulauan Seribu'] = '179';
		$city['Sungai Penuh'] = '509';

		$city['Kota Ambon'] = '424';
		$city['Kota Bandung'] = '165';
		$city['Kota Banda Aceh'] = '19';
		$city['Kota Bengkulu'] = '60';
		$city['Kota Bandar Lampung'] = '126';
		$city['Kota Banjarmasin'] = '322';
		$city['Kota Binjai'] = '45';
		$city['Kota Bukittinggi'] = '95';
		$city['Kota Batam'] = '139';
		$city['Kota Banjar'] = '166';
		$city['Kota Bekasi'] = '167';
		$city['Kota Bogor'] = '168';
		$city['Kota Batu'] = '244';
		$city['Kota Blitar'] = '245';
		$city['Kota Bima'] = '275';
		$city['Kota Banjarbaru'] = '321';
		$city['Kota Balikpapan'] = '347';
		$city['Kota Bontang'] = '348';
		$city['Kota Bau Bau'] = '389';
		$city['Kota Bitung'] = '409';
		$city['Kota Cilegon'] = '145';
		$city['Kota Cimahi'] = '169';
		$city['Kota Cirebon'] = '170';
		$city['Kota Denpasar'] = '266';
		$city['Kota Dumai'] = '81';
		$city['Kota Depok'] = '171';
		$city['Kota Gorontalo'] = '356';
		$city['Kota Gunungsitoli'] = '475';
		$city['Kota Jambi'] = '70';
		$city['Kota Jayapura'] = '462';
		$city['Kota Kupang'] = '295';
		$city['Kota Kendari'] = '390';
		$city['Kota Kediri'] = '246';
		$city['Kota Kotamobagu'] = '474';
		$city['Kota Langsa'] = '20';
		$city['Kota Lhokseumawe'] = '21';
		$city['Kota Lubuk Linggau'] = '113';
		$city['Kota Medan'] = '46';
		$city['Kota Mataram'] = '274';
		$city['Kota Makassar'] = '378';
		$city['Kota Manado'] = '410';
		$city['Kota Metro'] = '127';
		$city['Kota Magelang'] = '209';
		$city['Kota Madiun'] = '247';
		$city['Kota Malang'] = '248';
		$city['Kota Mojokerto'] = '249';
		$city['Kota Pekanbaru'] = '82';
		$city['Kota Padang'] = '96';
		$city['Kota Palembang'] = '115';
		$city['Kota Pangkal Pinang'] = '134';
		$city['Kota Pontianak'] = '308';
		$city['Kota Palangkaraya'] = '336';
		$city['Kota Palu'] = '400';
		$city['Kota Padangsidimpuan'] = '47';
		$city['Kota Pematang Siantar'] = '48';
		$city['Kota Padang Panjang'] = '97';
		$city['Kota Pariaman'] = '98';
		$city['Kota Payakumbuh'] = '99';
		$city['Kota Pagar Alam'] = '114';
		$city['Kota Prabumulih'] = '116';
		$city['Kota Pekalongan'] = '213';
		$city['Kota Pasuruan'] = '250';
		$city['Kota Probolinggo'] = '251';
		$city['Kota Palopo'] = '379';
		$city['Kota Pare Pare'] = '380';
		$city['Kota Pariaman'] = '520';
		$city['Kota Surabaya'] = '252';
		$city['Kota Serang'] = '147';
		$city['Kota Semarang'] = '212';
		$city['Kota Samarinda'] = '349';
		$city['Kota Sabang'] = '22';
		$city['Kota Subulussalam'] = '23';
		$city['Kota Sibolga'] = '49';
		$city['Kota Sawahlunto'] = '100';
		$city['Kota Solok'] = '101';
		$city['Kota Sukabumi'] = '172';
		$city['Kota Surakarta'] = '210';
		$city['Kota Salatiga'] = '211';
		$city['Kota Singkawang'] = '309';
		$city['Kota Sorong'] = '441';
		$city['Kota Sungai Penuh'] = '510';
		$city['Kota Tanjung Pinang'] = '140';
		$city['Kota Tanjung Balai'] = '50';
		$city['Kota Tebing Tinggi'] = '51';
		$city['Kota Tangerang'] = '146';
		$city['Kota Tasikmalaya'] = '173';
		$city['Kota Tegal'] = '214';
		$city['Kota Tarakan'] = '350';
		$city['Kota Tomohon'] = '411';
		$city['Kota Ternate'] = '431';
		$city['Kota Tidore'] = '432';
		$city['Kota Tangerang Selatan'] = '463';
		$city['Kota Tarakan'] = '470';
		$city['Kota Tual'] = '540';
		$city['Kota Tidore Kepulauan'] = '542';
		$city['Kota Tual'] = '577';
		$city['Kota Yogyakarta'] = '257';
		$city['Kota Denpasar Utara'] = '476';

		$city['Kab Maluku Tenggara Barat'] = '421';
		$city['Kab Pegunungan Bintang'] = '453';
		$city['Kab Maluku Tenggara Barat'] = '460';
		$city['Kab Aceh Barat'] = '1';
		$city['Kab Aceh Barat Daya'] = '2';
		$city['Kab Aceh Besar'] = '3';
		$city['Kab Aceh Jaya'] = '4';
		$city['Kab Aceh Selatan'] = '5';
		$city['Kab Aceh Singkil'] = '6';
		$city['Kab Aceh Tamiang'] = '7';
		$city['Kab Aceh Tengah'] = '8';
		$city['Kab Aceh Tenggara'] = '9';
		$city['Kab Aceh Timur'] = '10';
		$city['Kab Aceh Utara'] = '11';
		$city['Kab Asahan'] = '24';
		$city['Kab Agam'] = '83';
		$city['Kab Alor'] = '276';
		$city['Kab Asmat'] = '442';
		$city['Kab Bener Meriah'] = '12';
		$city['Kab Bireuen'] = '13';
		$city['Kab Batu Bara'] = '25';
		$city['Kab Bengkulu Selatan'] = '52';
		$city['Kab Bengkulu Utara'] = '53';
		$city['Kab Batanghari'] = '61';
		$city['Kab Bungo'] = '62';
		$city['Kab Bengkalis'] = '71';
		$city['Kab Banyuasin'] = '102';
		$city['Kab Bangka'] = '128';
		$city['Kab Bangka Barat'] = '129';
		$city['Kab Bangka Tengah'] = '130';
		$city['Kab Bangka Selatan'] = '131';
		$city['Kab Belitung'] = '132';
		$city['Kab Belitung Timur'] = '133';
		$city['Kab Bintan'] = '136';
		$city['Kab Bandung'] = '148';
		$city['Kab Bandung Barat'] = '149';
		$city['Kab Bekasi'] = '150';
		$city['Kab Bogor'] = '151';
		$city['Kab Banjarnegara'] = '180';
		$city['Kab Banyumas'] = '181';
		$city['Kab Batang'] = '182';
		$city['Kab Blora'] = '183';
		$city['Kab Boyolali'] = '184';
		$city['Kab Brebes'] = '185';
		$city['Kab Bangkalan'] = '215';
		$city['Kab Banyuwangi'] = '216';
		$city['Kab Blitar'] = '217';
		$city['Kab Bojonegoro'] = '218';
		$city['Kab Bondowoso'] = '219';
		$city['Kab Bantul'] = '253';
		$city['Kab Badung'] = '258';
		$city['Kab Bangli'] = '259';
		$city['Kab Buleleng'] = '260';
		$city['Kab Bima'] = '267';
		$city['Kab Belu'] = '277';
		$city['Kab Bengkayang'] = '296';
		$city['Kab Balangan'] = '310';
		$city['Kab Banjar'] = '311';
		$city['Kab Barito Kuala'] = '312';
		$city['Kab Barito Selatan'] = '323';
		$city['Kab Barito Timur'] = '324';
		$city['Kab Barito Utara'] = '325';
		$city['Kab Berau'] = '337';
		$city['Kab Bulungan'] = '338';
		$city['Kab Boalemo'] = '351';
		$city['Kab Bone Bolango'] = '352';
		$city['Kab Bantaeng'] = '357';
		$city['Kab Barru'] = '358';
		$city['Kab Bone'] = '359';
		$city['Kab Bulukumba'] = '360';
		$city['Kab Bombana'] = '381';
		$city['Kab Buton'] = '382';
		$city['Kab Banggai'] = '391';
		$city['Kab Banggai Kepulauan'] = '392';
		$city['Kab Buol'] = '393';
		$city['Kab Bolaang Mongondow'] = '401';
		$city['Kab Bolaang Mongondow Utara'] = '402';
		$city['Kab Buru'] = '417';
		$city['Kab Biak Numfor'] = '443';
		$city['Kab Boven Digoel'] = '444';
		$city['Kab Bengkulu Tengah'] = '464';
		$city['Kab Baturaja'] = '465';
		$city['Kab Bulungan'] = '466';
		$city['Kab Bolaang Mongondow Timur'] = '511';
		$city['Kab Banggai Laut'] = '534';
		$city['Kab Bolaang Mongondow Selatan'] = '536';
		$city['Kab Buru Selatan'] = '538';
		$city['Kab Buton Utara'] = '557';
		$city['Kab Buton Tengah'] = '570';
		$city['Kab Buton Selatan'] = '572';
		$city['Kab Buru Selatan'] = '579';
		$city['Kab Ciamis'] = '152';
		$city['Kab Cianjur'] = '153';
		$city['Kab Cirebon'] = '154';
		$city['Kab Cilacap'] = '186';
		$city['Kab Dairi'] = '26';
		$city['Kab Deli Serdang'] = '27';
		$city['Kab Dharmasraya'] = '84';
		$city['Kab Demak'] = '187';
		$city['Kab Dompu'] = '268';
		$city['Kab Donggala'] = '394';
		$city['Kab Deiyai'] = '549';
		$city['Kab Dogiyai'] = '550';
		$city['Kab Empat Lawang'] = '103';
		$city['Kab Ende'] = '278';
		$city['Kab Enrekang'] = '361';
		$city['Kab Flores Timur'] = '279';
		$city['Kab Fak Fak'] = '433';
		$city['Kab Gayo Lues'] = '14';
		$city['Kab Garut'] = '155';
		$city['Kab Grobogan'] = '188';
		$city['Kab Gresik'] = '220';
		$city['Kab Gunungkidul'] = '254';
		$city['Kab Gianyar'] = '261';
		$city['Kab Gunung Mas'] = '326';
		$city['Kab Gorontalo'] = '353';
		$city['Kab Gorontalo Utara'] = '354';
		$city['Kab Gowa'] = '362';
		$city['Kab Humbang Hasundutan'] = '28';
		$city['Kab Hulu Sungai Selatan'] = '313';
		$city['Kab Hulu Sungai Tengah'] = '314';
		$city['Kab Hulu Sungai Utara'] = '315';
		$city['Kab Halmahera Barat'] = '425';
		$city['Kab Halmahera Selatan'] = '426';
		$city['Kab Halmahera Tengah'] = '427';
		$city['Kab Halmahera Timur'] = '428';
		$city['Kab Halmahera Utara'] = '429';
		$city['Kab Indragiri Hilir'] = '72';
		$city['Kab Indragiri Hulu'] = '73';
		$city['Kab Indramayu'] = '156';
		$city['Kab Intan Jaya'] = '548';
		$city['Kab Jepara'] = '189';
		$city['Kab Jember'] = '221';
		$city['Kab Jombang'] = '222';
		$city['Kab Jembrana'] = '262';
		$city['Kab Jeneponto'] = '363';
		$city['Kab Jayapura'] = '445';
		$city['Kab Jayawijaya'] = '446';
		$city['Kab Karo'] = '29';
		$city['Kab Kaur'] = '54';
		$city['Kab Kepahiang'] = '55';
		$city['Kab Kerinci'] = '63';
		$city['Kab Kampar'] = '74';
		$city['Kab Kepulauan Meranti'] = '75';
		$city['Kab Kuantan Singingi'] = '76';
		$city['Kab Kepulauan Mentawai'] = '86';
		$city['Kab Karimun'] = '135';
		$city['Kab Karawang'] = '157';
		$city['Kab Kuningan'] = '158';
		$city['Kab Karanganyar'] = '190';
		$city['Kab Kebumen'] = '191';
		$city['Kab Kendal'] = '192';
		$city['Kab Klaten'] = '193';
		$city['Kab Kudus'] = '194';
		$city['Kab Kediri'] = '223';
		$city['Kab Kulon Progo'] = '255';
		$city['Kab Karangasem'] = '263';
		$city['Kab Klungkung'] = '264';
		$city['Kab Kupang'] = '280';
		$city['Kab Kapuas Hulu'] = '297';
		$city['Kab Ketapang'] = '298';
		$city['Kab Kayong Utara'] = '299';
		$city['Kab Kubu Raya'] = '307';
		$city['Kab Kotabaru'] = '316';
		$city['Kab Kapuas'] = '327';
		$city['Kab Katingan'] = '328';
		$city['Kab Kotawaringin Barat'] = '329';
		$city['Kab Kotawaringin Timur'] = '330';
		$city['Kab Kutai Barat'] = '339';
		$city['Kab Kutai Kartanegara'] = '340';
		$city['Kab Kutai Timur'] = '341';
		$city['Kab Kolaka'] = '383';
		$city['Kab Kolaka Utara'] = '384';
		$city['Kab Konawe'] = '385';
		$city['Kab Konawe Selatan'] = '386';
		$city['Kab Kepulauan Sangihe'] = '403';
		$city['Kab Kepulauan Talaud'] = '404';
		$city['Kab Kepulauan Aru'] = '418';
		$city['Kab Kepulauan Sula'] = '430';
		$city['Kab Kaimana'] = '434';
		$city['Kab Keerom'] = '447';
		$city['Kab Kepulauan Anambas'] = '514';
		$city['Kab Kepulauan Mentawai'] = '523';
		$city['Kab Kepulauan Selayar'] = '531';
		$city['Kab Konawe Kepulauan'] = '532';
		$city['Kab Kolaka Timur'] = '533';
		$city['Kab Kep. Siau Tagulandang Biaro'] = '537';
		$city['Kab Kepulauan Yapen'] = '547';
		$city['Kab Konawe Kepulauan'] = '568';
		$city['Kab Konawe Utara'] = '574';
		$city['Kab Labuhanbatu'] = '30';
		$city['Kab Langkat'] = '31';
		$city['Kab Lebong'] = '56';
		$city['Kab Lima Puluh Kota'] = '85';
		$city['Kab Lahat'] = '104';
		$city['Kab Lampung Barat'] = '117';
		$city['Kab Lampung Selatan'] = '118';
		$city['Kab Lampung Tengah'] = '119';
		$city['Kab Lampung Timur'] = '120';
		$city['Kab Lampung Utara'] = '121';
		$city['Kab Lingga'] = '137';
		$city['Kab Lebak'] = '141';
		$city['Kab Lamongan'] = '224';
		$city['Kab Lumajang'] = '225';
		$city['Kab Lombok Barat'] = '269';
		$city['Kab Lombok Tengah'] = '270';
		$city['Kab Lombok Timur'] = '271';
		$city['Kab Lembata'] = '281';
		$city['Kab Landak'] = '300';
		$city['Kab Lamandau'] = '331';
		$city['Kab Luwu'] = '364';
		$city['Kab Luwu Timur'] = '365';
		$city['Kab Luwu Utara'] = '366';
		$city['Kab Lombok Utara'] = '472';
		$city['Kab Labuhanbatu Utara'] = '516';
		$city['Kab Labuhanbatu Selatan'] = '517';
		$city['Kab Lanny Jaya'] = '553';
		$city['Kab Mandailing Natal'] = '32';
		$city['Kab Muko Muko'] = '57';
		$city['Kab Merangin'] = '64';
		$city['Kab Muaro Jambi'] = '65';
		$city['Kab Muara Enim'] = '105';
		$city['Kab Musi Banyuasin'] = '106';
		$city['Kab Musi Rawas'] = '107';
		$city['Kab Majalengka'] = '159';
		$city['Kab Magelang'] = '195';
		$city['Kab Madiun'] = '226';
		$city['Kab Magetan'] = '227';
		$city['Kab Malang'] = '228';
		$city['Kab Mojokerto'] = '229';
		$city['Kab Manggarai'] = '282';
		$city['Kab Manggarai Barat'] = '283';
		$city['Kab Manggarai Timur'] = '284';
		$city['Kab Melawi'] = '301';
		$city['Kab Murung Raya'] = '332';
		$city['Kab Malinau'] = '342';
		$city['Kab Maros'] = '367';
		$city['Kab Muna'] = '387';
		$city['Kab Morowali'] = '395';
		$city['Kab Minahasa'] = '405';
		$city['Kab Minahasa Utara'] = '406';
		$city['Kab Minahasa Selatan'] = '407';
		$city['Kab Minahasa Tenggara'] = '408';
		$city['Kab Majene'] = '412';
		$city['Kab Mamasa'] = '413';
		$city['Kab Mamuju'] = '414';
		$city['Kab Mamuju Utara'] = '415';
		$city['Kab Maluku Tengah'] = '419';
		$city['Kab Maluku Tenggara'] = '420';
		$city['Kab Manokwari'] = '435';
		$city['Kab Mappi'] = '448';
		$city['Kab Merauke'] = '449';
		$city['Kab Mimika'] = '450';
		$city['Kab Mamberamo Raya'] = '461';
		$city['Kab Malinau'] = '467';
		$city['Kab Mesuji'] = '513';
		$city['Kab Mahakam Ulu'] = '527';
		$city['Kab Mempawah'] = '528';
		$city['Kab Malaka'] = '529';
		$city['Kab Mamuju Tengah'] = '530';
		$city['Kab Morowali Utara'] = '535';
		$city['Kab Maluku Barat Daya'] = '539';
		$city['Kab Maybrat'] = '543';
		$city['Kab Manokwari Selatan'] = '544';
		$city['Kab Mamberamo Tengah'] = '555';
		$city['Kab Musi Rawas Utara'] = '558';
		$city['Kab Muna Barat'] = '573';
		$city['Kab Maluku Barat Daya'] = '578';
		$city['Kab Nagan Raya'] = '15';
		$city['Kab Nias'] = '33';
		$city['Kab Nias Selatan'] = '34';
		$city['Kab Natuna'] = '138';
		$city['Kab Nganjuk'] = '230';
		$city['Kab Ngawi'] = '231';
		$city['Kab Nagekeo'] = '285';
		$city['Kab Ngada'] = '286';
		$city['Kab Nunukan'] = '343';
		$city['Kab Nabire'] = '451';
		$city['Kab Nunukan'] = '468';
		$city['Kab Nias Utara'] = '518';
		$city['Kab Nias Barat'] = '519';
		$city['Kab Nduga'] = '554';
		$city['Kab Ogan Ilir'] = '108';
		$city['Kab Ogan Komering Ilir'] = '109';
		$city['Kab Ogan Komering Ulu'] = '110';
		$city['Kab Ogan Komering Ulu Timur'] = '111';
		$city['Kab Ogan Komering Ulu Selatan'] = '112';
		$city['Kab Pidie'] = '16';
		$city['Kab Pidie Jaya'] = '17';
		$city['Kab Padang Lawas'] = '35';
		$city['Kab Padang Lawas Utara'] = '36';
		$city['Kab Pakpak Bharat'] = '37';
		$city['Kab Pelalawan'] = '77';
		$city['Kab Padang Pariaman'] = '87';
		$city['Kab Pasaman'] = '88';
		$city['Kab Pasaman Barat'] = '89';
		$city['Kab Pesisir Selatan'] = '90';
		$city['Kab Pesawaran'] = '125';
		$city['Kab Pandeglang'] = '142';
		$city['Kab Purwakarta'] = '160';
		$city['Kab Pati'] = '196';
		$city['Kab Pekalongan'] = '197';
		$city['Kab Pemalang'] = '198';
		$city['Kab Purbalingga'] = '199';
		$city['Kab Purworejo'] = '200';
		$city['Kab Pacitan'] = '232';
		$city['Kab Pamekasan'] = '233';
		$city['Kab Pasuruan'] = '234';
		$city['Kab Ponorogo'] = '235';
		$city['Kab Probolinggo'] = '236';
		$city['Kab Pontianak'] = '302';
		$city['Kab Pulang Pisau'] = '333';
		$city['Kab Paser'] = '344';
		$city['Kab Penajam Paser Utara'] = '345';
		$city['Kab Pahuwato'] = '355';
		$city['Kab Pangkajene Kepulauan'] = '368';
		$city['Kab Pinrang'] = '369';
		$city['Kab Parigi Moutong'] = '396';
		$city['Kab Poso'] = '397';
		$city['Kab Polewali Mandar'] = '416';
		$city['Kab Paniai'] = '452';
		$city['Kab Puncak Jaya'] = '454';
		$city['Kab Pringsewu'] = '471';
		$city['Kab Pangandaran'] = '473';
		$city['Kab Padang Pariaman'] = '521';
		$city['Kab Pesisir Selatan'] = '522';
		$city['Kab Penukal Abab Lematang Ilir'] = '524';
		$city['Kab Pesisir Barat'] = '526';
		$city['Kab Pulau Morotai'] = '541';
		$city['Kab Pegunungan Arfak'] = '545';
		$city['Kab Puncak'] = '551';
		$city['Kab Pulau Taliabu'] = '580';
		$city['Kab Rejang Lebong'] = '58';
		$city['Kab Rokan Hulu'] = '78';
		$city['Kab Rokan Hilir'] = '79';
		$city['Kab Rembang'] = '201';
		$city['Kab Rote Ndao'] = '287';
		$city['Kab Raja Ampat'] = '436';
		$city['Kab Simeulue'] = '18';
		$city['Kab Samosir'] = '38';
		$city['Kab Serdang Bedagai'] = '39';
		$city['Kab Simalungun'] = '40';
		$city['Kab Seluma'] = '59';
		$city['Kab Sarolangun'] = '66';
		$city['Kab Siak'] = '80';
		$city['Kab Sijunjung'] = '91';
		$city['Kab Solok'] = '92';
		$city['Kab Solok Selatan'] = '93';
		$city['Kab Serang'] = '143';
		$city['Kab Subang'] = '161';
		$city['Kab Sukabumi'] = '162';
		$city['Kab Sumedang'] = '163';
		$city['Kab Semarang'] = '202';
		$city['Kab Sragen'] = '203';
		$city['Kab Sukoharjo'] = '204';
		$city['Kab Sampang'] = '237';
		$city['Kab Sidoarjo'] = '238';
		$city['Kab Situbondo'] = '239';
		$city['Kab Sumenep'] = '240';
		$city['Kab Sleman'] = '256';
		$city['Kab Sumbawa'] = '272';
		$city['Kab Sumbawa Barat'] = '273';
		$city['Kab Sikka'] = '288';
		$city['Kab Sumba Barat'] = '289';
		$city['Kab Sumba Barat Daya'] = '290';
		$city['Kab Sumba Tengah'] = '291';
		$city['Kab Sumba Timur'] = '292';
		$city['Kab Sambas'] = '303';
		$city['Kab Sanggau'] = '304';
		$city['Kab Sekadau'] = '305';
		$city['Kab Sintang'] = '306';
		$city['Kab Sukamara'] = '334';
		$city['Kab Seruyan'] = '335';
		$city['Kab Selayar'] = '370';
		$city['Kab Sinjai'] = '371';
		$city['Kab Sidenreng Rappang'] = '372';
		$city['Kab Soppeng'] = '373';
		$city['Kab Seram Bagian Barat'] = '422';
		$city['Kab Seram Bagian Timur'] = '423';
		$city['Kab Sorong'] = '437';
		$city['Kab Sorong Selatan'] = '438';
		$city['Kab Sarmi'] = '455';
		$city['Kab Supiori'] = '456';
		$city['Kab Sigi'] = '477';
		$city['Kab Sabu Raijua'] = '515';
		$city['Kab Tapanuli Selatan'] = '41';
		$city['Kab Tapanuli Tengah'] = '42';
		$city['Kab Tapanuli Utara'] = '43';
		$city['Kab Toba Samosir'] = '44';
		$city['Kab Tanjung Jabung Timur'] = '67';
		$city['Kab Tanjung Jabung Barat'] = '68';
		$city['Kab Tebo'] = '69';
		$city['Kab Tanah Datar'] = '94';
		$city['Kab Tanggamus'] = '123';
		$city['Kab Tulang Bawang'] = '124';
		$city['Kab Tangerang'] = '144';
		$city['Kab Tasikmalaya'] = '164';
		$city['Kab Tegal'] = '205';
		$city['Kab Temanggung'] = '206';
		$city['Kab Trenggalek'] = '241';
		$city['Kab Tuban'] = '242';
		$city['Kab Tulungagung'] = '243';
		$city['Kab Tabanan'] = '265';
		$city['Kab Timor Tengah Selatan'] = '293';
		$city['Kab Timor Tengah Utara'] = '294';
		$city['Kab Tanah Laut'] = '317';
		$city['Kab Tabalong'] = '318';
		$city['Kab Tanah Bumbu'] = '319';
		$city['Kab Tapin'] = '320';
		$city['Kab Tana Tidung'] = '346';
		$city['Kab Takalar'] = '374';
		$city['Kab Tana Toraja'] = '375';
		$city['Kab Toraja Utara'] = '376';
		$city['Kab Tojo Una Una'] = '398';
		$city['Kab Toli Toli'] = '399';
		$city['Kab Teluk Bintuni'] = '439';
		$city['Kab Teluk Wondama'] = '440';
		$city['Kab Tolikara'] = '457';
		$city['Kab Tana Tidung'] = '469';
		$city['Kab Tulang Bawang Barat'] = '525';
		$city['Kab Tambrauw'] = '546';
		$city['Kab Way Kanan'] = '122';
		$city['Kab Wonogiri'] = '207';
		$city['Kab Wonosobo'] = '208';
		$city['Kab Wajo'] = '377';
		$city['Kab Wakatobi'] = '388';
		$city['Kab Waropen'] = '458';
		$city['Kab Yahukimo'] = '459';
		$city['Kab Yalimo'] = '552';
		
		return $city;
	}
