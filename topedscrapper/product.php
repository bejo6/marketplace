<?php

	/* get product */
	function get_product_by_category($type, $built, $page, $rows, $params, $file){
		
		$params['sc']    = $built['cat_id'];
		$params['page']  = $page;
		$params['rows']  = $rows;
		$params['start'] = ($page * $rows) - $rows;

		
		$limit = 0;

		if ( isset($params['limit']) && is_numeric($params['limit']) ) {
			$limit = $params['limit'];
		}
		
		$new_url = build_link($params, $type);
		$data    = browsing($new_url);

		if( isJson($data) ){
			$result         = json_decode($data, true);
			$totalProduct   = ( isset($result['header']) && isset($result['header']['total_data']) ) ? $result['header']['total_data'] : 0;
			$totalPage      = ceil($totalProduct / $rows);

			if( !$totalProduct ){
				echo "[!] Product NOT FOUND!\n";
				return false;
			}else{
				echo "[+] Total All Product Category '{$built['cat_name']}' is {$totalProduct}\n";
				sleep(1);
			}

			$products       = $result['data']['products'];
			$count_products = count($products);
			
			if( $products ){
				
				echo "[+] Total Product Category '{$built['cat_name']}' page {$page} is {$count_products}\n";
				sleep(1);
				$i = 0;
				foreach($products as $product) {
					if ($saved = save_product($product, $file) ){
						$i++;
					}
					if ( $i > 0 && $limit == $i ){
						return 'limit';
					}
				}
				
				if( $count_products == $rows && $page < $totalPage){
					$page++;
					get_product_by_category($type, $built, $page, $rows, $params, $file);
				}
			}		
		}else{
			echo "[!] Can't get list product\n";
			return false;
		}
	}
	
	function get_product_by_seller($type, $built, $page, $rows, $params, $file){		
		
		$params['shop_id'] = $built['shop_id'];
		$params['rows']    = $rows;
		$params['start']   = ($page * $rows) - $rows;
		
		$limit = 0;

		if ( isset($params['limit']) && is_numeric($params['limit']) ) {
			$limit = $params['limit'];
		}
		
		$new_url = build_link($params, $type);
		$data    = browsing($new_url);

		if( isJson($data) ){
			$result         = json_decode($data, true);
			$totalProduct   = ( isset($result['header']) && isset($result['header']['total_data']) ) ? $result['header']['total_data'] : 0;
			$totalPage      = ceil($totalProduct / $rows);

			if( !$totalProduct ){
				echo "[!] Product NOT FOUND! Exit...\n";
				return false;
			}else{
				echo "[+] Total All Product Seller '{$built['shop_name']}' is {$totalProduct}\n";
				sleep(1);
			}

			$products       = $result['data']['products'];
			$count_products = count($products);
			
			if( $products ){
				
				echo "[+] Total Product Seller '{$built['shop_name']}' Page {$page} : is {$count_products}\n";
				sleep(1);
				$i = 0;

				foreach($products as $product) {
					if( $saved = save_product($product, $file) ){
						$i++;
					}
					if ( $i > 0 && $limit == $i ){
						return 'limit';
					}
				}
				if( $count_products == $rows && $page < $totalPage){
					$page++;
					get_product_by_seller($type, $built, $page, $rows, $params, $file);
				}
			}		
		}else{
			echo "[!] Can't get list product\n";
			return false;
		}
	}
	
	function get_hot_product($type, $built, $page, $rows, $params, $file){
		foreach($built as $k=>$v){
			if( !preg_match('/type|file|ob|hot_name/', $k) ){
				if( !isset($params[$k]) ){
					$params[$k] = $v;
				}
			}
		}

		$params['rows']        = $rows;
		$params['start']       = ($page * $rows) - $rows;
		
		$limit = 0;

		if ( isset($params['limit']) && is_numeric($params['limit']) ) {
			$limit = $params['limit'];
		}
		
		$new_url = build_link($params, $type);
		$data    = browsing($new_url);

		if( isJson($data) ){
			$result         = json_decode($data, true);
			$totalProduct   = ( isset($result['header']) && isset($result['header']['total_data']) ) ? $result['header']['total_data'] : 0;
			$totalPage      = ceil($totalProduct / $rows);

			if( !$totalProduct ){
				echo "[!] Product NOT FOUND! Exit...\n";
				return false;
			}else{
				echo "[+] Total All Hot Product '{$built['hot_name']}' is {$totalProduct}\n";
				sleep(1);
			}

			$products       = $result['data']['products'];
			$count_products = count($products);
			
			if( $products ){
				
				echo "[+] Total Hot Product '{$built['hot_name']}' Page {$page} : is {$count_products}\n";
				sleep(1);
				$i = 0;

				foreach($products as $product) {
					if( $saved = save_product($product, $file) ){
						$i++;
					}
					if ( $i > 0 && $limit == $i ){
						return 'limit';
					}
				}
				if( $count_products == $rows && $page < $totalPage){
					$page++;
					get_hot_product($type, $built, $page, $rows, $params, $file);
				}
			}		
		}else{
			echo "[!] Can't get list product\n";
			return false;
		}
	}

	function get_detail_product($data){
		$product = array();
		
		
		$url         = bersih_url($data['url']);
		echo "[+] Get data {$url}\n";

		$single      = browsing($url);
		$html        = str_get_html($single);
		
        if ( !preg_match( '/tokopedia\.com/i', $single) || preg_match( '/Page Not Found/i', $single) ) {
            echo "[!] Gagal mendapatkan data produk...\n";
            return false;
        }
		
		//if ( !is_stock_available($html, $data['name']) ) return false;

		$scripts     = get_product_script($html);
		$images      = get_product_images($html, $single, $data['image_url']);
		$description = get_product_description($html, $single);

		$weight      = get_product_weight($html, $single);
		$categories  = get_product_categories($data['department_id'], $scripts);
		$sold        = get_product_sold($data['id']);
		$rating      = get_product_rating($data['id']);

		if( !$images || !$description || !$weight  || !$categories ){
			return false;
		}

		
		$product['id']    = $data['id'];
		$product['url']   = $url;
		$product['name']  = $data['name'];
		$product['price'] = $data['price_int'];
		if( trim($data['original_price']) ){
			$product['price'] = preg_replace('/\D+/', '', $data['original_price']);
		}
		$product['thumbnail_1']  = $images['thumbnail_1'];
		$product['thumbnail_2']  = $images['thumbnail_2'];
		$product['thumbnail_3']  = $images['thumbnail_3'];
		$product['thumbnail_4']  = $images['thumbnail_4'];
		$product['thumbnail_5']  = $images['thumbnail_5'];
		$product['thumbnail_6']  = $images['thumbnail_6'];
		$product['thumbnail_7']  = $images['thumbnail_7'];
		$product['thumbnail_8']  = $images['thumbnail_8'];
		$product['thumbnail_9']  = $images['thumbnail_9'];
		$product['thumbnail_10'] = $images['thumbnail_10'];
		
        $product['description']      = $description; // get description
        $product['description_html'] = '';
        $product['weight']           = $weight;
        $product['condition']        = ($data['condition'] == 1) ? 'Baru' : 'Bekas';
        $product['min_order']        = $data['min_order'];
        $product['cat_1']            = $categories['cat_1'];
        $product['cat_2']            = $categories['cat_2'];
        $product['cat_3']            = $categories['cat_3'];
        $product['sold']             = $sold;
        $product['views']            = 0;
        $product['rating']           = $rating['rating'];
        $product['rating_by']        = $rating['rating_by'];
		
		/* optional for full data */
        $product['seller_id']        = $data['shop']['id'];
        $product['seller_name']      = $data['shop']['name'];
        $product['seller_url']       = $data['shop']['url'];
        $product['seller_gold']      = ($data['shop']['is_gold'] == true) ? '1' : '0';
        $product['seller_official']  = ($data['shop']['is_official'] == true) ? '1' : '0';
		
		return $product;
	}
	
	function is_stock_available($html, $name){
		if( $html ){
			if( $html->find('div[class=rvm-button--buy]') ){
				return true;
			}
		}
		if( $html ){
			if( $html->find('div[class="alert alert-block alert-block-not-available"]') ){
				echo "[!] Stock Product {$name} Kosong\n";
				return false;
			}
		}

		return true;
	}

	function get_product_images($html, $str, $default){
		$list_images = array();
		$images      = array();

		if( $div_images = $html->find('div[class=content-img-relative]') ){			
			foreach( $div_images as $div) {
				$img = $div->find('img', 0);

				if( $img && isset($img->src) ){
					$list_images[] = $img->src;
				}
			}
		}

		if( !$list_images ){
			if( preg_match_all('/[<\s]div\s+class[\s=]+(["\'])content-img-relative(\g1)>\s+<img\s+src[\s=]+(\g1)(.*?)(\g1)[\s\/>]+/smi', $str, $match)){
				if( isset($match[4]) && $match[4] ){
					foreach($match[4] as $img) {
						$list_images[] = $img;
					}
				}
			}
		}

		if( !$list_images ){
			$list_images[] = $default;
		}
		
		$list_images = array_values(array_unique($list_images));
		
        for($i=0; $i<10; $i++) {
			$pkey = $i + 1;
			$key = "thumbnail_{$pkey}";
			$images[$key] = '';

            if( isset($list_images[$i]) ){
				$images[$key] = $list_images[$i];
			}
        }

		return $images;

	}
	
	function get_product_description($html, $str){
		$description = false;
		
		$info = $html->find('div[id=info]');
		foreach($info as $div){
			if($div->attr['itemprop'] == 'description'){
				$description = trim($div->plaintext);
				break;
			}
		}
		
		if( !$description ){
			if( preg_match('/[<\s]div\s+id[\s=]+(["\'])info(\g1)(.*?)itemprop[\s=]+(\g1)description(\g1)[>\s]+(.*?)<[\s\/]div>/smi', $str, $match)){
				if( isset($match[6]) && (null !== $match[6]) ){
					$description = preg_replace('/([<\s]br[\s\/]>)/', "\n", trim($match[6]));
				}
			}
		}

		return html_entity_decode($description);

	}

	function get_product_weight($html, $str){
		$get_weight = false;

		if( $shipping_content = trim($html->find('div[class=rvm-shipping-content]', 0)->plaintext) ){
			$get_weight = $shipping_content;
		}
		
		
		if( !$get_weight){
			if( preg_match('/[<\s]div\s+class[\s=]+(["\'])rvm-shipping-content(\g1)>\s+(.*?)<[\s\/]div>/smi', $str, $match)){
				if( isset($match[3]) && (null !== $match[3]) ){
					$get_weight = trim($match[3]);
				}
			}
		}
		
		if( $get_weight ){
			
			$weight = preg_replace('/\D+/', '' , $get_weight);
			
			if( preg_match('/gr/i', $get_weight) ){
				return $weight;
			}
			
			if( preg_match('/kg/i', $get_weight) ){
				return 1000 * $weight;
			}
		}

		return false;

	}
	
	function get_product_categories($catid, $scripts){
		$result = array();
		if ( $cat = get_category_toped($catid) ) {
			if( isset($cat['parent']['name']) ){
				$result['cat_1'] = $cat['parent']['name'];
				if( isset($cat['sub']['name']) ){
					$result['cat_2'] = $cat['sub']['name'];
					$result['cat_3'] = $cat['name'];
				}elseif( isset($cat['name']) ){
					$result['cat_2'] = $cat['name'];
					$result['cat_3'] = '';
				}

			}elseif( isset($cat['name']) ){
				$result['cat_1'] = $cat['name'];
				$result['cat_2'] = '';
				$result['cat_3'] = '';
			}

		} elseif ( $scripts ) {
			if ( $cat = get_product_category_full($scripts) ) {
				for ( $i=0;$i<count($cat);$i++ ) {
					$key = 'cat_' . ($i+1);
					$result[$key] = $cat[$i];
				}
			}
		}

		return $result;

	}
	
	function get_product_sold($id){
		$sold = 0;
		$stat = browsing('https://js.tokopedia.com/productstats/check?pid='.$id.'&callback=show_product_stats&_='.strtotime(date('Y-m-d H:i:s')));
		if( preg_match('/(["\'])item_sold(\g1)[:\s]+(\d+)[,\s]+(\g1)/', $stat, $match) ){
			if( isset($match[3]) && (null !== $match[3]) ){
				$sold = trim($match[3]);
			}
		}

		return $sold;

	}
	
	function validate_csv_file($file, $fullname){
		$csv_toped = "url,name,price,thumbnail_1,thumbnail_2,thumbnail_3,thumbnail_4,thumbnail_5,thumbnail_6,thumbnail_7,thumbnail_8,thumbnail_9,thumbnail_10,description,description_html,weight,condition,min_order,cat_1,cat_2,cat_3,sold,views,rating,rating_by";
		
		$csv_full = "id,url,name,price,thumbnail_1,thumbnail_2,thumbnail_3,thumbnail_4,thumbnail_5,thumbnail_6,thumbnail_7,thumbnail_8,thumbnail_9,thumbnail_10,description,description_html,weight,condition,min_order,cat_1,cat_2,cat_3,sold,views,rating,rating_by,seller_id,seller_name,seller_url,seller_gold,seller_official";
		
		if( !file_exists($file) ) {
			$fl = fopen($file, 'w');
			fwrite($fl, "{$csv_toped}\n");
			fclose($fl);
		}
		
		if( FULL ) {
			if( !file_exists($fullname) ) {
				$fl = fopen($fullname, 'w');
				fwrite($fl, "{$csv_full}\n");
				fclose($fl);
			}
		}
		
		/* Mass Shopee format
		
		ps_category_list_id
		ps_product_name
		ps_product_description
		ps_price
		ps_stock
		ps_product_weight
		ps_days_to_ship
		ps_sku_ref_no_parent
		ps_mass_upload_variation_help
		ps_variation 1 ps_variation_sku
		ps_variation 1 ps_variation_name
		ps_variation 1 ps_variation_price
		ps_variation 1 ps_variation_stock
		ps_variation 2 ps_variation_sku
		ps_variation 2 ps_variation_name
		ps_variation 2 ps_variation_price
		ps_variation 2 ps_variation_stock
		ps_variation 3 ps_variation_sku
		ps_variation 3 ps_variation_name
		ps_variation 3 ps_variation_price
		ps_variation 3 ps_variation_stock
		ps_variation 4 ps_variation_sku
		ps_variation 4 ps_variation_name
		ps_variation 4 ps_variation_price
		ps_variation 4 ps_variation_stock
		ps_variation 5 ps_variation_sku
		ps_variation 5 ps_variation_name
		ps_variation 5 ps_variation_price
		ps_variation 5 ps_variation_stock
		ps_variation 6 ps_variation_sku
		ps_variation 6 ps_variation_name
		ps_variation 6 ps_variation_price
		ps_variation 6 ps_variation_stock
		ps_variation 7 ps_variation_sku
		ps_variation 7 ps_variation_name
		ps_variation 7 ps_variation_price
		ps_variation 7 ps_variation_stock
		ps_variation 8 ps_variation_sku
		ps_variation 8 ps_variation_name
		ps_variation 8 ps_variation_price
		ps_variation 8 ps_variation_stock
		ps_variation 9 ps_variation_sku
		ps_variation 9 ps_variation_name
		ps_variation 9 ps_variation_price
		ps_variation 9 ps_variation_stock
		ps_variation 10 ps_variation_sku
		ps_variation 10 ps_variation_name
		ps_variation 10 ps_variation_price
		ps_variation 10 ps_variation_stock
		ps_variation 11 ps_variation_sku
		ps_variation 11 ps_variation_name
		ps_variation 11 ps_variation_price
		ps_variation 11 ps_variation_stock
		ps_variation 12 ps_variation_sku
		ps_variation 12 ps_variation_name
		ps_variation 12 ps_variation_price
		ps_variation 12 ps_variation_stock
		ps_variation 13 ps_variation_sku
		ps_variation 13 ps_variation_name
		ps_variation 13 ps_variation_price
		ps_variation 13 ps_variation_stock
		ps_variation 14 ps_variation_sku
		ps_variation 14 ps_variation_name
		ps_variation 14 ps_variation_price
		ps_variation 14 ps_variation_stock
		ps_variation 15 ps_variation_sku
		ps_variation 15 ps_variation_name
		ps_variation 15 ps_variation_price
		ps_variation 15 ps_variation_stock
		ps_variation 16 ps_variation_sku
		ps_variation 16 ps_variation_name
		ps_variation 16 ps_variation_price
		ps_variation 16 ps_variation_stock
		ps_variation 17 ps_variation_sku
		ps_variation 17 ps_variation_name
		ps_variation 17 ps_variation_price
		ps_variation 17 ps_variation_stock
		ps_variation 18 ps_variation_sku
		ps_variation 18 ps_variation_name
		ps_variation 18 ps_variation_price
		ps_variation 18 ps_variation_stock
		ps_variation 19 ps_variation_sku
		ps_variation 19 ps_variation_name
		ps_variation 19 ps_variation_price
		ps_variation 19 ps_variation_stock
		ps_variation 20 ps_variation_sku
		ps_variation 20 ps_variation_name
		ps_variation 20 ps_variation_price
		ps_variation 20 ps_variation_stock
		ps_img_1
		ps_img_2
		ps_img_3
		ps_img_4
		ps_img_5
		ps_img_6
		ps_img_7
		ps_img_8
		ps_img_9
		ps_mass_upload_shipment_help
		channel 80066 switch
		channel 80003 switch
		channel 80004 switch
		channel 80005 switch
		channel 80011 switch
		channel 80014 switch
		channel 80012 switch
		channel 80013 switch
		channel 80013 switch

		*/
		
		/* Bukalapak format
		
		Nama Barang
		Stok(Minimum 1)
		Berat (gram)
		Harga (Rupiah)
		Kondisi(Baru/Bekas)
		Deskripsi
		Wajib Asuransi?(Ya/Tidak)
		Jasa Pengiriman (gunakan vertical bar | sebagai pemisah jasa pengiriman contoh: jner | jney)
		URL Gambar 1
		URL Gambar 2
		URL Gambar 3
		URL Gambar 4
		URL Gambar 5
		
		*/

	}

    function save_product($data_product, $file = 'file.csv', $shopee = false, $bukalapak = false) {
		
        $url      = bersih_url($data_product['url']);
        $row      = 1;
        $data     = array();
        $limit    = 100000;
        $fullname = "data/FULL-{$file}";
        $file     = "data/{$file}";

        if( !validate_path($file) ){
			exit;
		}
		
        if(is_sudah($url)) {
            echo "[-] Product sudah pernah di proses...\n";
            return false;
        }
				
		$product  = get_detail_product($data_product);
		if ( !$product ) return false;
		
		validate_csv_file($file, $fullname);
		
		$handle  = fopen($file,"r");
		while(! feof($handle) && $row <= $limit) {
			$data[] = fgetcsv($handle);
			$row++;
		}
		fclose($handle);

		foreach($data as $key => $row) {
			if($product['name'] == $row[1]) {
				echo "[-] Product {$product['name']} sudah ada...\n";
				return false;
			}
		}
		

		if( FULL ){
			$fp = fopen($fullname, 'a');
			fputcsv($fp, $product);
			fclose($fp);
		}
		
		unset($product['id']);
		unset($product['seller_id']);
		unset($product['seller_name']);
		unset($product['seller_url']);
		unset($product['seller_gold']);
		unset($product['seller_official']);

		$fp = fopen($file, 'a');
		fputcsv($fp, $product);
		fclose($fp);
		
		echo "[+] Saved to CSV...Total Product " . (count($data)-1). "\n";
        
        sleep(1);

		return true;
    }
	
	function get_product_rating($id){
		$result = array();
        $result['rating']           = 0;
        $result['rating_by']        = 0;
		
		$reputation = browsing('https://www.tokopedia.com/reputationapp/review/api/v1/rating?product_id=' . $id);

		if( isJson($reputation) ){
			$json = json_decode($reputation, true);
			$result['rating']           = $json['data']['rating_score'];
			$result['rating_by']        = $json['data']['total_review'];
		}else{
			if( preg_match('/(["\'])rating_score(\g1)[:\s]+(\g1)(.*?)(\g1)[,\s]+(\g1)/', $reputation, $match) ){
				if( isset($match[4]) && (null !== $match[4]) ){
					$result['rating'] = trim($match[4]);
				}
			}
			if( preg_match('/(["\'])total_review(\g1)[:\s]+(\d+)[,\s]+(\g1)/', $reputation, $match) ){
				if( isset($match[3]) && (null !== $match[3]) ){
					$result['rating_by'] = trim($match[3]);
				}
			}			
		}

		return $result;

	}
	
	function get_sub_category($scripts){
		$categories      = array();

		if( $scripts ){
			foreach($scripts as $script){

				if( preg_match('/(["\'])subcategory_id(\g1)[\s:]+(\g1)(.*?)?(\g1)[\s\+]+(\d+)[\s,]+/i', $script, $match) ){
					if( isset($match[6]) && null !== $match[6] ){
						$categories['id'] = trim($match[6]);
					}
				}
				
				if( preg_match('/(["\'])subcategory(\g1)[:\s]+(\g1)(.*?)(\g1)[\s,]+/i', $script, $match) ){
					if( isset($match[4]) && null !== $match[4] ){
						$categories['name'] = trim($match[4]);
					}
				}
			}
		}
		
		return $categories;

	}
	
	function get_product_category_full($scripts){
		$categories      = array();
		$category_tree   = array();
		$cat_main_id     = null;
		$cat_main_name   = null;
		$cat_sub_id      = null;
		$cat_sub_name    = null;
		$cat_parent_id   = null;
		$cat_parent_name = null;
		
		if( is_array($scripts) && !empty($scripts) ){
			foreach($scripts as $script){

				if( preg_match('/(^|\s+)var\s+category_id[\s=]+(["\'])(.*?)(\g2)[\s;]+var\s+category_tree/smi', $script, $match) ){
					if( isset($match[3]) && null !== $match[3] ){
						$cat_main_id = $match[3];
					}
				}
				
				if( preg_match('/(^|\s+)var\s+category_tree[\s=]+(["\'])(.*?)(\g2)[\s;]+/smi', $script, $match)){
					if( isset($match[3]) && null !== $match[3] ){
						$category_tree = array_filter(array_map('trim', explode("/", $match[3])));
					}
				}
				
				if( preg_match('/(["\'])subcategory_id(\g1)[\s:]+(\g1)(.*?)?(\g1)[\s\+]+(\d+)[\s,]+/i', $script, $match) ){
					if( isset($match[6]) && null !== $match[6] ){
						$cat_sub_id = trim($match[6]);
					}
				}
				
				if( preg_match('/(["\'])subcategory(\g1)[:\s]+(\g1)(.*?)(\g1)[\s,]+/i', $script, $match) ){
					if( isset($match[4]) && null !== $match[4] ){
						$cat_sub_name = trim($match[4]);
					}
				}
				
				if( preg_match('/(["\'])category_id(\g1)[\s:]+(\g1)(.*?)?(\g1)[\s\+]+(\d+)[\s,]+/i', $script, $match) ){
					if( isset($match[6]) && null !== $match[6] ){
						$cat_parent_id = trim($match[6]);
					}
				}
				
				if( preg_match('/(["\'])category(\g1)[:\s]+(\g1)(.*?)(\g1)[\s,]+/i', $script, $match) ){
					if( isset($match[4]) && null !== $match[4] ){
						$cat_parent_name = trim($match[4]);
					}
				}
			}
			
			if( $cat_parent_id && $cat_parent_name ){
				$categories[$cat_parent_id] = $cat_parent_name;
			}
			
			if( $cat_sub_id && $cat_sub_name ){
				$categories[$cat_sub_id] = $cat_sub_name;
			}
			
			if( $cat_main_id && $category_tree ){
				$categories[$cat_main_id] = end($category_tree);
			}
		}
		
		return $categories;

	}
	
	function get_product_id($html, $scripts){
		$product_id = null;
		
		if ( $scripts ) {
			foreach($scripts as $script){
				if( preg_match('/product_id[\s=]+(\d+)[\s;]+/', $script, $match) ){
					$product_id = $match[1];
					break;
				} elseif ( preg_match('/product_stats_ajax_script[\s=]+(["\'])https?:\/\/js\.tokopedia\.com\/productstats\/check\?pid=(\d+)(\g1)[\s;]+/', $script, $match) ){
					$product_id = $match[2];
					break;
				} elseif ( preg_match('/product_id[\s=]+(["\'])(\d+)(\g1)[\s;]+/', $script, $match) ){
					$product_id = $match[2];
					break;
				} elseif ( preg_match('/product_view_ajax_script[\s=]+(["\'])https?:\/\/www\.tokopedia\.com\/provi\/check\?pid=(\d+)(\g1)[\s;]+/', $script, $match) ){
					$product_id = $match[2];
					break;
				} elseif ( preg_match('/(["\'])product_id(\g1)[\s:]+(\g1)(.*?)?(\g1)[\s\+]+(\d+)[\s,]+/', $script, $match) ){
					if( isset($match[6]) && (null !== $match[6]) ){
						$product_id = $match[6];
						break;
					}
				} elseif ( preg_match('/(["\'])product_deeplink_url(\g1)[\s:]+(\g1)tokopedia:\/\/product\/(\d+)(\g1)[\s,]+/', $script, $match) ){
					if( isset($match[4]) && (null !== $match[4]) ){
						$product_id = $match[4];
						break;
					}
				}

			}
		}
		
		if( !$product_id) {
			if( $input_id = trim($html->find('input[id=product-id]', 0)->attr['value']) ){
				$product_id = $input_id;
			}
		
		}

		if( !$product_id) {
			if( $container = $html->find('div[class="container container-product"]') ){
				foreach($container as $val){
					if( isset($val->attr['id']) ){
						if( preg_match('/product-(\d+)/i', $val->attr['id'], $match) ){
							$product_id = trim($match[1]);
							break;
						}
					}

				}
			}			
		}

		if( !$product_id) {
			if( $meta_property = $html->find('meta[property="al:ios:url"]', 0)->attr['content'] ){
				$product_id = trim(preg_replace('/tokopedia:\/\/product\/|\D+/', '', $meta_property));
			}			
		}

		if( !$product_id) {
			if( $meta_name = $html->find('meta[name="branch:deeplink:$ios_deeplink_path"]', 0)->attr['content'] ){
				$product_id = trim(preg_replace('/product\/|\D+/', '', $meta_name));
			} elseif ( $meta_name = $html->find('meta[name="branch:deeplink:$android_deeplink_path"]', 0)->attr['content'] ){
				$product_id = trim(preg_replace('/product\/|\D+/', '', $meta_name));
			}
		}


		return $product_id;
	}
	
	function get_product_name($html, $scripts, $str){
		$name = null;

		// with html dom
		
		if( $h1 = trim($html->find('h1[class=rvm-product-title]', 0)->children(0)->plaintext) ){
			$name    = $h1;
		} elseif ( $id_product_name = trim($html->find('h1[class=rvm-product-title]', 0)->children(0)->plaintext) ) {
			$name    = $id_product_name;
		} elseif ( $id_MessageApp = trim(html_entity_decode($html->find('div[id=MessageApp]', 0)->attr['receiver-subject'])) ) {
			$name    = $id_MessageApp;
		} elseif ( $meta_twitter = trim($html->find('meta[name=twitter:title]', 0)->attr['content']) ) {
			$name    = $meta_twitter;
		} elseif ( $breadcrumb = trim($html->find('div[id=breadcrumb-container]', 0)->children(0)->find('li[class=active]', 0)->plaintext) ) {
			$name    = $breadcrumb;
		}
		
        // with regex

		if( !$name && $scripts){
			foreach($scripts as $script){
				if( preg_match('/(["\'])product_name(\g1)[\s:]+(\g1)(.*?)(\g1)[,\s]+/i', $script, $match) ){
					if( isset($match[4]) && (null !== $match[4]) ){
						$name = trim($match[4]);
						break;
					}
				}
			}
		}

		if( !$name ){
			if( preg_match('/[<\s]h1\s+class[\s=]+(["\'])rvm-product-title(\g1)>\s+<span\s+itemprop[\s=]+(\g1)name(\g1)>(.*?)<\/span>\s+<\/h1>/smi', $str, $match)){
				if( isset($match[5]) && (null !== $match[5]) ){
					$name = trim($match[5]);
				}
			} elseif ( preg_match('/[<\s]input\s+type[\s=]+(["\'])hidden(\g1)\s+name[\s=]+(\g1)product_name(\g1)\s+id[\s=]+(\g1)product-name(\g1)\s+value[\s=]+(\g1)(.*?)(\g1)[\s\/>]+/i', $str, $match) ){
				if( isset($match[8]) && (null !== $match[8]) ){
					$name = trim(html_entity_decode($match[8]));
				}
			} elseif ( preg_match('/[\s"\']receiver-subject=(["\'])(.*?)(\g1)[\s>]/i', $str, $match) ){
				if( isset($match[2])  && (null !== $match[2])){
					$name = trim(html_entity_decode($match[2]));
				}
			} elseif ( preg_match('/[<\s]meta\s+name[\s=]+(["\'])twitter:title(\g1)\s+content[\s=]+(\g1)(.*?)(\g1)[\s\/>]+/i', $str, $match)){
				$name = trim($match[4]);
			} elseif ( preg_match_all('/[<\s]+li\s+class[\s=]+(["\'])active(\g1)[>\s]+<h2>(.*?)<\/h2>\s+<\/li>/smi', $str, $match)){
				if( isset($match[3]) && (null !== $match[3]) ){
					$name = trim($match[3][0]);
				}
			}
		}

		return $name;

	}
	
	