<?php

    set_time_limit(0);
    
    define("PROJECT", "grabber");
    define("FULL", false);

    include 'function.php';
    include 'options.php';
    include 'simple_html_dom.php';
    include 'config.php';
    include 'product.php';

    $limit  = null;
    $params  = get_options($argv[0]);

	myheader($argv[0], $params);

	if( isset($params['list']) ){
		$list_file = $params['list'];
		
		if( !file_exists($list_file) ){
			echo "[!] File {$list_file} not exists!!!\n";
			exit;
		}
		
		if( $list = bacaFile($list_file) ){
			if( $links = array_filter(array_map('trim', explode("\n", $list))) ){
				foreach($links as $link){
					$x = execute($link, $params);
					if($x == 'limit'){
						exit;
					}
				}
				exit;
			}
		}
	}
	
	if( isset($params['url']) ){
		$url = $params['url'];
		$x = execute($url, $params);
		if($x == 'limit'){
			exit;
		}
	}


	function execute($url, $params){
		$built = prepare($url);
		if( !$built ){
			echo "[!] Something wrong\n";
			echo "[!] Pleace check url\n\n";
			return false;
		}
		
		$file = $built['file'];
		$type = $built['type'];
		
		if(isset($params['file'])){
			$file = "{$params['file']}.csv";
		}
		
		$page = 1;


		$rows = 200;

		if($type == 'seller'){
			echo "[+] Start scrapping Seller '{$built['shop_name']}', Seller ID : {$built['shop_id']} ...\n";
			echo "[+] URL : {$url}\n";

			$params = link_to_param( $url, $params );
			if(isset($params['page'])){
				$page = $params['page'];
			}
			return get_product_by_seller($type, $built, $page, $rows, $params, $file);
		}
		
		if($type == 'category'){
			echo "[+] Start scrapping category '{$built['cat_name']}', Category ID : {$built['cat_id']} ...\n";
			echo "[+] URL : {$url}\n";
			
			$params = link_to_param( $url, $params );
			if(isset($params['page'])){
				$page = $params['page'];
			}
			return get_product_by_category($type, $built, $page, $rows, $params, $file);
		}
		
		if($type == 'hot'){
			echo "[+] Start scrapping Hot Product '{$built['hot_name']}' ...\n";
			echo "[+] URL : {$url}\n";
			
			$params = link_to_param( $url, $params );
			if(isset($params['page'])){
				$page = $params['page'];
			}
			
			return get_hot_product($type, $built, $page, $rows, $params, $file);
		}
	}
