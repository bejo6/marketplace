<?php

#Scrape per kategori Keep It Simple, Stupid method.
#ID kategori&child dari https://hades.tokopedia.com/v0/categories/ 


$terjual = 30; #minimal produk terjual
$kategori = 'perawatan-tubuh'; 
$kategoriid = '2133';
$jumlahmax = 2000; #produk yang ingin diambil

$agents = array(
	'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1',
	'Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
	'Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36',
	'Mozilla/5.0 (Android 7.0; Mobile; rv:54.0) Gecko/54.0 Firefox/54.0'
 
);

#$aHTTP['http']['header']  = "User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1\r\n";
$aHTTP['http']['header']  = "User-Agent: ".$agents[array_rand($agents)];
$aHTTP['http']['header'] .= "Referer: https://www.tokopedia.com/\r\n";

			
$context  = stream_context_create($aHTTP);

for ($i= 0; $i <= $jumlahmax; $i=$i+60){
	#filter: jabodetabek, bintang 4&5,  JNE, baru, sort produk terbaru, sila diganti jika perlu.
	$url = 'https://ace.tokopedia.com/search/product/v3?scheme=https&device=mobile&related=true&start='.$i.'&ob=5&source=directory&st=product&identifier='.$kategori.'&fcity=144,146,150,151,167,168,171,174,175,176,177,178,463&condition=1&rt=4,5&shipping=1&sc='.$kategoriid.'&rows=60';


	$data = json_decode(file_get_contents($url, false, $context));
	$hotlist = $data->data->products;


	foreach ($hotlist as $key) {
		$produkurl = str_replace('https://m.tokopedia.com', 'https://www.tokopedia.com', $key->url);
		$produkurl = strtok($produkurl, "?");
		$parentid = $key->parent_id;
		$stats = file_get_contents('https://js.tokopedia.com/productstats/check?pid='.$key->id, false, $context);
		$stats = str_replace('"', '', $stats);
		$start = "show_product_stats({item_sold:";
		$end = ", success:";
		$laku = getBetween($stats,$start,$end);
		
		$urlspeed = 'https://www.tokopedia.com/reputationapp/statistic/api/v1/shop/'.$key->shop->id.'/speed';
		
		
		if($laku >=$terjual ){
			$cekspeed = json_decode(file_get_contents($urlspeed, false, $context));
			$speed = $cekspeed->data->speed->recent_1_month->speed_level;
			if($speed == 5 ){ #kecepatan toko 5 = sangat cepat
				echo "proses: " . $produkurl . PHP_EOL;
				echo "terjual: " . $laku . PHP_EOL;
				echo "kecepatan toko: ".$speed." OK" .  PHP_EOL;
				file_put_contents('TP_'.$kategori.'.txt', $produkurl . PHP_EOL, FILE_APPEND | LOCK_EX);
			}
			else {
				echo "proses: " . $produkurl . PHP_EOL;
				echo "terjual: " . $laku . PHP_EOL;
				echo "kecepatan toko: ".$speed." BAD" .  PHP_EOL;
			}
		}
		elseif($laku == 0 ){
			$cekspeed = json_decode(file_get_contents($urlspeed, false, $context));
			$speed = $cekspeed->data->speed->recent_1_month->speed_level;
			$statsparent = file_get_contents('https://js.tokopedia.com/productstats/check?pid='.$parentid, false, $context);
			$statsparent = str_replace('"', '', $statsparent);
			$lakuparent = getBetween($statsparent,$start,$end);
			if($lakuparent >=$terjual ){
				if($speed == 5 ){ #kecepatan toko 5 = sangat cepat
					echo "proses: " . $produkurl . PHP_EOL;
					echo "terjual: " . $lakuparent . PHP_EOL;
					echo "kecepatan toko: ".$speed." OK" .  PHP_EOL;
					file_put_contents('TP_'.$kategori.'.txt', $produkurl . PHP_EOL, FILE_APPEND | LOCK_EX);
				}
				else {
					echo "proses: " . $produkurl . PHP_EOL;
					echo "terjual: " . $lakuparent . PHP_EOL;
					echo "kecepatan toko: ".$speed." BAD" .  PHP_EOL;
				}
			}

			
		}
		else {
				echo "proses: " . $produkurl . PHP_EOL;
				echo "Skip, penjualan:  " . $laku . PHP_EOL;
			}
		
	}
	
}


function getBetween($stats,$start,$end){
    $r = explode($start, $stats);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}
